<?php

namespace backend\services;

use common\extensions\constant\CacheConstant;
use Yii;
use common\extensions\jwt\FirebaseJwt;
use common\models\AdminTokenModel;
use Yjius\common\ToolsHelper;

class AdminAuthService
{
    //解析jwt是否有效
    public static function checkLogin($jwt)
    {
        if (empty($jwt)) {
            throw new \Exception("登录验证信息不能为空");
        }
        //如果库里存在当前token未过期，才进行下一步
//        $exist = AdminTokenModel::getOne([['=', 'token' => $jwt], ['>', 'expire_date', date('Y-m-d H:i:s')]], 'token');
//        if (empty($exist)) {
//            throw new \Exception("登录验证信息已失效");
//        }
        if (!Yii::$app->cache->exists(CacheConstant::ADMIN_USER_TOKEN . $jwt)) {
            throw new \Exception("登录验证信息已失效");
        }
        //解析token
        return FirebaseJwt::decode($jwt);
    }

    //生成jwt内容
    public static function generateToken($userData)
    {
        $jwtData = FirebaseJwt::encode([
            "id" => $userData['id'],
            "username" => $userData['username'],
            "mobile" => $userData['mobile'],
            "create_time" => $userData['create_time']
        ]);

        //写入缓存
        Yii::$app->cache->set(CacheConstant::ADMIN_USER_TOKEN . $jwtData['jwt'], 1, FirebaseJwt::$expireTime);
        //写入token表
//        AdminTokenModel::saveDataNoExistAdd(
//            [
//                "user_id" => $userData['id'],
//                "token" => $jwtData['jwt'],
//                "expire_date" => date("Y-m-d H:i:s", $jwtData['payload']['exp']),
//                "update_date" => date("Y-m-d H:i:s", $jwtData['payload']['nbf'])
//            ], ['user_id' => $userData['id']]
//        );
        return $jwtData['jwt'];
    }

}