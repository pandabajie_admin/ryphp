<?php

namespace backend\controllers;

use Yii;
use yii\rest\Controller;

/**
 * Site controller
 */
class ErrorHandlerController extends Controller
{
    public function actionError()
    {
        return jsonErrorReturn("404", "接口不存在或发生未知错误！");
    }
}
