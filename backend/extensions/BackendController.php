<?php

namespace backend\extensions;

use backend\services\AdminAuthService;
use Yii;
use common\core\BaseController;

class BackendController extends BaseController
{
    //当前登录用户
    protected $adminUserInfo = [];
    protected $jwt = "";

    //不需要验证登录信息的接口白名单
    protected $noNeedLoginRoute = [];

    public function init()
    {
        parent::init();
        if (!in_array(Yii::$app->requestedRoute, $this->noNeedLoginRoute) && !$this->checkLogin()) {
            return jsonErrorReturn("needLogin");
        }
    }

    private function checkLogin()
    {
        //对token进行验证
        $token = \Yii::$app->request->headers->get("Authorization");
        //从get参数获取
        $token = $token ? $token : \Yii::$app->request->get("token");
        $token = str_replace('Bearer ', '', $token);
        try {
            $this->adminUserInfo = AdminAuthService::checkLogin($token);
            $this->jwt = $token;
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
