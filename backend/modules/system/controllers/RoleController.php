<?php

namespace backend\modules\system\controllers;

use common\extensions\helpers\TreeNewHelpers;
use common\models\AdminDeptModel;
use common\models\AdminRoleModel;
use common\models\AdminUserModel;
use common\models\RoleDeptModel;
use common\models\RoleMenuModel;
use common\models\UserRoleModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\Debug;
use Yjius\common\ToolsHelper;

class RoleController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = [];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'list' => ['GET'],
            'create-role' => ['POST'],
            'detail' => ['GET'],
            'update-role' => ['POST'],
            'delete-role' => ['POST'],
            'data-scope' => ['POST'],
            'dept-tree' => ['GET'],
            'allocated-auth-user-list' => ['GET'],
            'unallocated-auth-user-list' => ['GET'],
            'cancel-auth-user' => ['GET'],
            'cancel-all-auth-user' => ['GET'],
            'select-all-auth-user' => ['GET'],
            'change-status' => ['POST'],
        ];
    }

    //角色列表
    public function actionList()
    {
        $page = loadParam("pageNum");
        $limit = loadParam("pageSize");
        $roleName = loadParam("roleName");
        $roleKey = loadParam("roleKey");
        $status = loadParam("status", "");
        $params = loadParam("params");
        //搜素条件
        $where = ['and'];
        array_push($where, ['is_delete' => 0]);

        if (!empty($roleName)) {
            array_push($where, ['like', 'a.role_name', $roleName]);
        }
        if (!empty($roleKey)) {
            array_push($where, ['=', 'a.role_key', $roleKey]);
        }
        if (is_numeric($status)) {
            array_push($where, ["=", "a.status", $status]);
        }
        if (!empty($params['beginTime'])) {
            array_push($where, [">=", "a.create_time", $params['beginTime']]);
        }
        if (!empty($params['endTime'])) {
            array_push($where, ["<=", "a.create_time", $params['endTime']]);
        }
        $dataList = AdminRoleModel::getPageList($where, ["a.*"], $page, $limit, "a.role_sort asc");
        foreach ($dataList['list'] as &$item) {
            $item['role_id'] = (int)$item['id'];
        }
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($dataList));
    }

    //创建角色
    public function actionCreateRole()
    {
        $menuCheckStrictly = loadParam('menuCheckStrictly', "");
        $deptCheckStrictly = loadParam('deptCheckStrictly', "");
        $roleKey = loadParam('roleKey', "");

        $exist = AdminRoleModel::find()->where(['role_key' => $roleKey])->one();
        if (!empty($exist)) {
            return jsonErrorReturn("fail", "已存在相同的权限标识");
        }

        $menuData = [
            'role_name' => loadParam('roleName', ''),
            'role_key' => $roleKey,
            'role_sort' => loadParam('roleSort', 0),
            'menu_check_strictly' => !empty($menuCheckStrictly) ? 1 : 0,
            'dept_check_strictly' => !empty($deptCheckStrictly) ? 1 : 0,
            'status' => loadParam('status', 0),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
            'create_time' => date("Y-m-d H:i:s"),
            'create_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $createId = AdminRoleModel::saveDataNoExistAdd($menuData);
        $menuIds = loadParam('menuIds', []);
        if (!empty($createId)) {
            $batchInsert = [];
            foreach ($menuIds as $menuId) {
                $batchInsert[] = ['role_id' => $createId, 'menu_id' => $menuId];
            }
            if (!empty($batchInsert)) {
                RoleMenuModel::batchInsertMain($batchInsert);
            }

            return jsonSuccessReturn(['menuId' => $createId], "新增成功");
        } else {
            return jsonErrorReturn("fail", "新增失败");
        }
    }

    //更新角色
    public function actionUpdateRole()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        $menuCheckStrictly = loadParam('menuCheckStrictly', "");
        $deptCheckStrictly = loadParam('deptCheckStrictly', "");
        $roleKey = loadParam('roleKey', "");

        $exist = AdminRoleModel::find()->where(['role_key' => $roleKey])->andWhere(['!=', 'id', $roleId])->one();
        if (!empty($exist)) {
            return jsonErrorReturn("fail", "已存在相同的权限标识");
        }
        $menuData = [
            'role_name' => loadParam('roleName', ''),
            'role_key' => $roleKey,
            'role_sort' => loadParam('roleSort', 0),
            'menu_check_strictly' => !empty($menuCheckStrictly) ? 1 : 0,
            'dept_check_strictly' => !empty($deptCheckStrictly) ? 1 : 0,
            'status' => loadParam('status', 0),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $createId = AdminRoleModel::saveDataNoExistAdd($menuData, ['id' => $roleId]);
        $menuIds = loadParam('menuIds', []);
        if (!empty($createId)) {
            RoleMenuModel::deleteAll(['role_id' => $createId]);
            $batchInsert = [];
            foreach ($menuIds as $menuId) {
                $batchInsert[] = ['role_id' => $createId, 'menu_id' => $menuId];
            }
            if (!empty($batchInsert)) {
                RoleMenuModel::batchInsertMain($batchInsert);
            }

            return jsonSuccessReturn(['roleId' => $createId], "编辑成功");
        } else {
            return jsonErrorReturn("fail", "编辑失败");
        }
    }

    //角色详情
    public function actionDetail()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        $role = AdminRoleModel::getOne(["id" => $roleId]);
        $role['roleId'] = $role['id'];
        $role['menu_check_strictly'] = !empty($role['menu_check_strictly']) ? true : false;
        $role['dept_check_strictly'] = !empty($role['dept_check_strictly']) ? true : false;

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($role));
    }

    //删除角色
    public function actionDeleteRole()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        AdminRoleModel::updateAll(['is_delete' => 1], ["id" => $roleId]);
        return jsonSuccessReturn([], "删除成功");
    }

    //角色树形关系
    public function actionDeptTree()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }

        $checkedList = RoleDeptModel::find()->select(['dept_id'])->where(["role_id" => $roleId])->asArray()->all();
        $checkedKeys = array_column($checkedList, 'dept_id');

        $query = AdminDeptModel::find()->select(["id", "parent_id", "dept_name as label"])->where(['is_delete' => 0]);
        $depts = $query->orderBy("order_num asc")->asArray()->all();

        TreeNewHelpers::instance()->init($depts, 'parent_id');
        $treeList = TreeNewHelpers::instance()->getTreeArray2(0, '', false);

        $res = ['depts' => $treeList, 'checkedKeys' => $checkedKeys];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));

    }

    //分配部门数据权限
    public function actionDataScope()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        $dataScope = loadParam('dataScope', '');
        if ($dataScope != 1) {
            return jsonErrorReturn("fail", "目前仅支持”全部数据权限”，相关功能尚未实现");
        }
        $menuData = [
            'data_scope' => $dataScope,
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $operateId = AdminRoleModel::saveDataNoExistAdd($menuData, ['id' => $roleId]);
        $deptIds = loadParam('deptIds', []);
        if (!empty($operateId)) {
            RoleDeptModel::deleteAll(['role_id' => $operateId]);
            $batchInsert = [];
            foreach ($deptIds as $dept) {
                $batchInsert[] = ['role_id' => $operateId, 'dept_id' => $dept];
            }
            if (!empty($batchInsert)) {
                RoleDeptModel::batchInsertMain($batchInsert);
            }

            return jsonSuccessReturn(['roleId' => $roleId], "编辑成功");
        } else {
            return jsonErrorReturn("fail", "编辑失败");
        }
    }

    //已分配角色的用户
    public function actionAllocatedAuthUserList()
    {
        $page = loadParam("pageNum");
        $limit = loadParam("pageSize");
        $username = loadParam("username");
        $mobile = loadParam("mobile");
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        // 查询数据
        $query = AdminUserModel::find()->alias('a')->select(['b.*', 'a.id', 'a.username', 'a.nickname', 'a.email', 'a.mobile', 'a.create_time', 'a.status'])
            ->leftJoin(UserRoleModel::tableName() . ' as b', "a.id=b.user_id")
            ->where(['b.role_id' => $roleId, 'a.is_delete' => 0]);
        if (!empty($username)) {
            $query->andWhere(['like', 'a.username', $username]);
        }
        if (!empty($mobile)) {
            $query->andWhere(['=', 'a.mobile', $mobile]);
        }
        $count = $query->count();
        $count = intval($count);

        $offset = ($page - 1) * $limit;
        $query->offset($offset)->limit($limit);

        $list = $query->orderBy('a.id desc')->asArray()->all();


        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion(['count' => $count, 'list' => $list]));
    }

    //未分配角色的用户
    public function actionUnallocatedAuthUserList()
    {
        $page = loadParam("pageNum");
        $limit = loadParam("pageSize");
        $roleId = loadParam("roleId");
        $username = loadParam("username");
        $mobile = loadParam("mobile");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        $where = ['and'];
        array_push($where, ['a.is_delete' => 0]);

        $authUsers = UserRoleModel::find()->select('user_id')->where(['role_id' => $roleId])->asArray()->all();
        if (!empty($authUsers)) {
            $authUsers = array_column($authUsers, 'user_id');
            array_push($where, ['not in', 'a.id', $authUsers]);
        }

        if (!empty($username)) {
            array_push($where, ['like', 'a.username', $username]);
        }
        if (!empty($mobile)) {
            array_push($where, ['=', 'a.mobile', $mobile]);
        }

        $dataList = AdminUserModel::getPageList($where, ['a.id as user_id', 'a.username', 'a.nickname', 'a.email', 'a.mobile', 'a.create_time', 'a.status'], $page, $limit);
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($dataList));
    }

    //取消授权用户
    public function actionCancelAuthUser()
    {
        $userId = loadParam("userId");
        $roleId = loadParam("roleId");
        if (empty($roleId) || empty($userId)) {
            return jsonErrorReturn("paramsError");
        }
        UserRoleModel::deleteAll(['role_id' => $roleId, 'user_id' => $userId]);
        return jsonSuccessReturn([], "取消成功");
    }

    //批量取消授权用户
    public function actionCancelAllAuthUser()
    {
        $userIds = loadParam("userIds");
        $roleId = loadParam("roleId");
        if (empty($roleId) || empty($userIds)) {
            return jsonErrorReturn("paramsError");
        }
        $userIdArr = explode(',', $userIds);
        UserRoleModel::deleteAll(['and', ['role_id' => $roleId], ['in', 'user_id', $userIdArr]]);
        return jsonSuccessReturn([], "取消成功");
    }

    //添加授权用户
    public function actionSelectAllAuthUser()
    {
        $userIds = loadParam("userIds");
        $roleId = loadParam("roleId");
        if (empty($roleId) || empty($userIds)) {
            return jsonErrorReturn("paramsError");
        }
        $userIdArr = explode(',', $userIds);

        $batchInsert = [];
        foreach ($userIdArr as $userid) {
            $batchInsert[] = ['role_id' => $roleId, 'user_id' => $userid];
        }
        if (!empty($batchInsert)) {
            UserRoleModel::batchInsertMain($batchInsert);
        }

        return jsonSuccessReturn([], "操作成功");
    }

    //更新状态
    public function actionChangeStatus()
    {
        $roleId = loadParam("roleId");
        $status = loadParam("status");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }
        AdminRoleModel::updateAll(['status' => $status], ["id" => $roleId]);
        return jsonSuccessReturn([], "删除成功");
    }

}
