<?php

namespace backend\modules\system\controllers;

use common\models\AdminDeptModel;
use common\models\AdminMenuModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\Debug;
use Yjius\common\ToolsHelper;

class DeptController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = [];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'list' => ['GET'],
            'List-exclude' => ['GET'],
            'detail' => ['GET'],
            'create-dept' => ['POST'],
            'update-dept' => ['POST'],
            'delete-dept' => ['POST'],
        ];
    }

    //部门列表
    public function actionList()
    {
        $deptName = loadParam("deptName");
        $status = loadParam("status", "");
        $query = AdminDeptModel::find()->where(['is_delete' => 0]);
        if (!empty($deptName)) {
            $query->andWhere(["like", "dept_name", $deptName]);
        }
        if (is_numeric($status)) {
            $query->andWhere(["=", "status", $status]);
        }
        $deptList = $query->orderBy("order_num asc")->asArray()->all();
        foreach ($deptList as &$dept) {
            $dept['dept_id'] = $dept['id'];
        }
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($deptList));
    }

    //部门列表2
    public function actionListExclude()
    {
        $deptId = loadParam("deptId");
        $query = AdminDeptModel::find()->where(['is_delete' => 0]);
        if (!empty($deptId)) {
            $query->andWhere(["!=", "id", $deptId]);
        }
        $deptList = $query->orderBy("order_num asc")->asArray()->all();
        foreach ($deptList as &$dept) {
            $dept['dept_id'] = $dept['id'];
        }
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($deptList));
    }

    //部门详情
    public function actionDetail()
    {
        $deptId = loadParam("deptId");
        if (empty($deptId)) {
            return jsonErrorReturn("paramsError");
        }
        $menu = AdminDeptModel::getOne(["id" => $deptId]);
        $menu['dept_id'] = $menu['id'];

        $parent = AdminDeptModel::getOne(["id" => $menu['parent_id']], ['dept_name']);
        $menu['parent_name'] = $parent['dept_name'];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($menu));
    }

    //删除部门
    public function actionDeleteDept()
    {
        $deptId = loadParam("deptId");
        if (empty($deptId)) {
            return jsonErrorReturn("paramsError");
        }
        $children = AdminDeptModel::getOne(["parent_id" => $deptId], ['id']);
        if (!empty($children)) {
            return jsonErrorReturn("fail", "当前部门下仍有下级，无法删除。");
        }
        AdminDeptModel::updateAll(['is_delete' => 1], ["id" => $deptId]);
        return jsonSuccessReturn([], "删除成功");
    }

    //更新部门
    public function actionUpdateDept()
    {
        $deptId = loadParam("deptId");
        if (empty($deptId)) {
            return jsonErrorReturn("paramsError");
        }
        //找出所有上级
        $parentId = loadParam('parentId', 0);
        $parentList = AdminDeptModel::getParentDeptList($parentId);
        $parentIdArr = array_column($parentList, "parent_id");
        $parentIdArr[] = $parentId;
        $ancestors = implode(',', $parentIdArr);
        $deptData = [
            'ancestors' => $ancestors,
            'parent_id' => $parentId,
            'order_num' => loadParam('orderNum', 0),
            'dept_name' => loadParam('deptName', ""),
            'leader' => loadParam('leader', ""),
            'phone' => loadParam('phone', ""),
            'email' => loadParam('email', ""),
            'status' => loadParam('status', 0),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $updateId = AdminDeptModel::saveDataNoExistAdd($deptData, ['id' => $deptId]);
        if (!empty($updateId)) {
            return jsonSuccessReturn(['deptId' => $deptId], "编辑成功");
        } else {
            return jsonErrorReturn("fail", "编辑失败");
        }
    }

    //创建部门
    public function actionCreateMenu()
    {
        //找出所有上级
        $parentId = loadParam('parentId', 0);
        $parentList = AdminDeptModel::getParentDeptList($parentId);
        $parentIdArr = array_column($parentList, "parent_id");
        $parentIdArr[] = $parentId;
        $ancestors = implode(',', $parentIdArr);
        $menuData = [
            'ancestors' => $ancestors,
            'parent_id' => loadParam('parentId', 0),
            'order_num' => loadParam('orderNum', 0),
            'dept_name' => loadParam('deptName', ""),
            'leader' => loadParam('leader', ""),
            'phone' => loadParam('phone', ""),
            'email' => loadParam('email', ""),
            'status' => loadParam('status', 0),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
            'create_time' => date("Y-m-d H:i:s"),
            'create_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $createId = AdminDeptModel::saveDataNoExistAdd($menuData);
        if (!empty($createId)) {
            return jsonSuccessReturn(['deptId' => $createId], "新增成功");
        } else {
            return jsonErrorReturn("fail", "新增失败");
        }
    }

}
