<?php

namespace backend\modules\system\controllers;

use common\models\SystemDictModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\ToolsHelper;

class DictController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = [];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'list' => ['GET'],
            'detail' => ['GET'],
            'create' => ['POST'],
            'update' => ['POST'],
            'delete' => ['POST'],
            'refresh-cache' => ['POST'],
        ];
    }

    //角色列表
    public function actionList()
    {
        $page = loadParam("pageNum");
        $limit = loadParam("pageSize");
        $status = loadParam("status", "");
        $dictName = loadParam("dictName");
        $dictKey = loadParam("dictKey");
        $dateRange = loadParam("params");
        //搜素条件
        $where = ['and'];
        array_push($where, ['is_delete' => 0]);

        if (!empty($dictName)) {
            array_push($where, ['like', 'a.dict_name', $dictName]);
        }
        if (!empty($dictKey)) {
            array_push($where, ['=', 'a.dict_key', $dictKey]);
        }
        if (is_numeric($status)) {
            array_push($where, ["=", "a.status", $status]);
        }
        if (!empty($dateRange['beginTime'])) {
            array_push($where, [">=", "a.create_time", $dateRange['beginTime']]);
        }
        if (!empty($dateRange['endTime'])) {
            array_push($where, ["<=", "a.create_time", $dateRange['endTime']]);
        }
        $dataList = SystemDictModel::getPageList($where, ["a.*"], $page, $limit, "a.id asc");
        foreach ($dataList['list'] as &$item) {
            $item['dict_id'] = (int)$item['id'];
            $item['dict_content'] = json_decode($item['dict_content'], true);
        }
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($dataList));
    }

    //创建
    public function actionCreate()
    {
        $dictKey = loadParam('dictKey', "");
        $dictName = loadParam('dictName', "");
        if (empty($dictName) || empty($dictKey)) {
            return jsonErrorReturn("paramsError");
        }
        $exist = SystemDictModel::find()->where(['dict_key' => $dictKey])->one();
        if (!empty($exist)) {
            return jsonErrorReturn("fail", "已存在相同的字典标识");
        }

        $dictContent = loadParam('dictContent', "");
        $dictContent = json_encode($dictContent, JSON_UNESCAPED_UNICODE);

        $createData = [
            'dict_name' => $dictName,
            'dict_key' => $dictKey,
            'dict_content' => $dictContent,
            'status' => loadParam('status', 0),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
            'create_time' => date("Y-m-d H:i:s"),
            'create_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $createId = SystemDictModel::saveDataNoExistAdd($createData);
        if (!empty($createId)) {
            return jsonSuccessReturn(['dictId' => $createId], "新增成功");
        } else {
            return jsonErrorReturn("fail", "新增失败");
        }
    }

    //更新
    public function actionUpdate()
    {
        $dictId = loadParam("dictId");
        $dictKey = loadParam('dictKey', "");
        $dictName = loadParam('dictName', "");
        if (empty($dictId) || empty($dictName) || empty($dictKey)) {
            return jsonErrorReturn("paramsError");
        }
        $exist = SystemDictModel::find()->where(['dict_key' => $dictKey])->andWhere(['!=', 'id', $dictId])->one();
        if (!empty($exist)) {
            return jsonErrorReturn("fail", "已存在相同的字典标识");
        }
        $dictContent = loadParam('dictContent', "");
        $dictContent = json_encode($dictContent, JSON_UNESCAPED_UNICODE);

        $updateData = [
            'dict_name' => $dictName,
            'dict_key' => $dictKey,
            'dict_content' => $dictContent,
            'status' => loadParam('status', 0),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
        ];

        $dictId = SystemDictModel::saveDataNoExistAdd($updateData, ['id' => $dictId]);
        if (!empty($dictId)) {
            return jsonSuccessReturn(['dictId' => $dictId], "编辑成功");
        } else {
            return jsonErrorReturn("fail", "编辑失败");
        }
    }

    //角色详情
    public function actionDetail()
    {
        $dictId = loadParam("dictId");
        if (empty($dictId)) {
            return jsonErrorReturn("paramsError");
        }
        $detail = SystemDictModel::getOne(["id" => $dictId]);
        $detail['dict_content'] = json_decode($detail['dict_content'], true);
        $detail['dict_id'] = (int)$detail['id'];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($detail));
    }

    //删除角色
    public function actionDelete()
    {
        $dictId = loadParam("dictId");
        if (empty($dictId)) {
            return jsonErrorReturn("paramsError");
        }
        SystemDictModel::updateAll(['is_delete' => 1], ["id" => $dictId]);
        return jsonSuccessReturn([], "删除成功");
    }

    public function actionRefreshCache()
    {
        return jsonSuccessReturn([], "缓存刷新成功");
    }
}
