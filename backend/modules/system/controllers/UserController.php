<?php

namespace backend\modules\system\controllers;

use common\extensions\fileupload\FileUploader;
use common\extensions\helpers\TreeNewHelpers;
use common\models\AdminDeptModel;
use common\models\AdminMenuModel;
use common\models\AdminRoleModel;
use common\models\AdminUserModel;
use common\models\RoleDeptModel;
use common\models\RoleMenuModel;
use common\models\UserRoleModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\Debug;
use Yjius\common\RegexHelper;
use Yjius\common\ToolsHelper;

class UserController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = [];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'user-info' => ['GET'],
            'dept-tree' => ['GET'],
            'list' => ['GET'],
            'detail' => ['GET'],
            'change-status' => ['POST'],
            'create-user' => ['POST'],
            'update-user' => ['POST'],
            'delete-user' => ['POST'],
            'reset-pwd' => ['POST'],
            'auth-role' => ['GET'],
            'save-auth-role' => ['POST'],
            'profile' => ['GET'],
            'update-profile' => ['POST'],
            'update-profile-pwd' => ['POST'],
        ];
    }

    //用户信息
    public function actionUserInfo()
    {
        //处理当前用户的角色
        $roles = UserRoleModel::find()->alias('a')
            ->select(["id as role_id", "role_name", "role_key", "data_scope"])
            ->leftJoin(AdminRoleModel::tableName() . ' as b', 'a.role_id=b.id')
            ->where(['b.is_delete' => 0, 'status' => 0, 'a.user_id' => $this->adminUserInfo['id']])->asArray()->all();
        $res['roles'] = array_column($roles, "role_key");
        //获取所在角色下的所有菜单
        if (in_array("admin", $res['roles'])) {
            $res['permissions'] = ["*:*:*"];
        } else {
            //获取菜单权限
            $roleIdArr = array_column($roles, "role_id");
            $roleMenu = RoleMenuModel::find()->alias('a')
                ->select(["id as menu_id", "menu_name", "perms"])
                ->leftJoin(AdminMenuModel::tableName() . ' as b', 'a.menu_id=b.id')
                ->where(['in', 'role_id', $roleIdArr])
                ->asArray()->all();

            $res['permissions'] = array_column($roleMenu, "perms");
            $res['permissions'] = array_filter($res['permissions'], function ($value) {
                return $value !== '';
            });
            $res['permissions'] = array_values($res['permissions']);
        }
        $admin = AdminUserModel::getOne(['id' => $this->adminUserInfo['id']]);
        unset($admin['password']);
        unset($admin['salt']);
        $res['user'] = $admin;
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));
    }

    //部门的树形结构
    public function actionDeptTree()
    {
        //TODO 用户数据权限
        $query = AdminDeptModel::find()->select(["id", "parent_id", "dept_name as label"])->where(['is_delete' => 0]);
        $depts = $query->orderBy("order_num asc")->asArray()->all();

        TreeNewHelpers::instance()->init($depts, 'parent_id');
        $treeList = TreeNewHelpers::instance()->getTreeArray2(0, '', false);

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($treeList));

    }

    //用户列表
    public function actionList()
    {
        $page = loadParam("pageNum");
        $limit = loadParam("pageSize");
        $status = loadParam("status", "");
        $username = loadParam("username");
        $mobile = loadParam("mobile");
        $params = loadParam("params");
        $where = ['and'];
        array_push($where, ['is_delete' => 0]);

        $query = AdminUserModel::find()->alias('a')->select(['b.dept_name', 'a.dept_id', 'a.id as user_id', 'a.username', 'a.nickname', 'a.email', 'a.mobile', 'a.create_time', 'a.status'])
            ->leftJoin(AdminDeptModel::tableName() . ' as b', "a.dept_id=b.id")
            ->where(['a.is_delete' => 0]);

        if (!empty($username)) {
            $query->andWhere(['like', 'a.username', $username]);
        }
        if (!empty($mobile)) {
            $query->andWhere(['=', 'a.mobile', $mobile]);
        }
        if (is_numeric($status)) {
            $query->andWhere(["=", "a.status", $status]);
        }
        if (!empty($params['beginTime'])) {
            $query->andWhere([">=", "a.create_time", $params['beginTime']]);
        }
        if (!empty($params['endTime'])) {
            $query->andWhere(["<=", "a.create_time", $params['endTime']]);
        }
        $count = $query->count();
        $count = intval($count);

        $offset = ($page - 1) * $limit;
        $query->offset($offset)->limit($limit);

        $list = $query->orderBy('a.id desc')->asArray()->all();

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion(['count' => $count, 'list' => $list]));
    }

    //更改状态
    public function actionChangeStatus()
    {
        $userId = loadParam("userId");
        $status = loadParam("status");
        if (empty($userId)) {
            return jsonErrorReturn("paramsError");
        }
        AdminUserModel::updateAll(['status' => $status], ["id" => $userId]);
        return jsonSuccessReturn([]);
    }

    //用户详情
    public function actionDetail()
    {
        $res = [];
        $userId = loadParam("userId", "");
        if (!empty($userId)) {
            $userRoles = UserRoleModel::find()->select(['role_id'])->where(["user_id" => $userId])->asArray()->all();
            $roleIds = array_column($userRoles, 'role_id');
            $res['roleIds'] = $roleIds;

            $res['userInfo'] = AdminUserModel::getOne(['id' => $userId]);
            $res['userInfo']['user_id'] = (int)$res['userInfo']['id'] ?? 0;
        }

        $roles = AdminRoleModel::find()->select(['id as roleId', 'role_name', "role_key", "status"])->where(["is_delete" => 0])->orderBy("role_sort asc")->asArray()->all();
        $res['roles'] = $roles;
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));
    }

    //创建用户
    public function actionCreateUser()
    {
        $username = loadParam("username", "");
        $nickname = loadParam("nickname", "");
        $mobile = loadParam("mobile", "");
        $password = loadParam("password", "");
        if (empty($mobile) || empty($username) || empty($password) || empty($nickname)) {
            return jsonErrorReturn("paramsError", "昵称,用户名、手机号、密码不能为空");
        }
        if (!RegexHelper::isPWD($password)) {
            return jsonErrorReturn("paramsError", "密码长度至少 6-18 位，必须包含有大写字母、小写字母、数字");
        }
        //当前用户已存在
        $userData = AdminUserModel::getOne(['username' => $username]);
        if (!empty($userData)) {
            return jsonErrorReturn("fail", "用户名username唯一，相同用户名信息已存在");
        }
        $userRegData = [
            "dept_id" => loadParam("deptId", 0),
            "username" => $username,
            'nickname' => $nickname,
            'email' => loadParam("email", ""),
            "mobile" => $mobile,
            'sex' => loadParam("sex", 0),
            'status' => loadParam("status", 0),
            "create_time" => date("Y-m-d H:i:s"),
            "create_by" => $this->adminUserInfo['username'],
            "salt" => AdminUserModel::generateSalt(),
            "remark" => loadParam("remark", ""),
        ];
        $userRegData["password"] = AdminUserModel::generatePassword($password, $userRegData['salt']);
        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData);
        if (!empty($userRegData["id"])) {
            $roleIds = loadParam("roleIds", []);
            if (!empty($roleIds)) {
                $batchInsert = [];
                foreach ($roleIds as $roleId) {
                    $batchInsert[] = ['role_id' => $roleId, 'user_id' => $userRegData["id"]];
                }
                if (!empty($batchInsert)) {
                    UserRoleModel::batchInsertMain($batchInsert);
                }
            }
        }

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($userRegData), '添加成功');
    }

    //更新用户
    public function actionUpdateUser()
    {
        $userId = loadParam("userId", 0);
        $username = loadParam("username", "");
        $nickname = loadParam("nickname", "");
        $mobile = loadParam("mobile", "");
        if (empty($userId) || empty($mobile) || empty($username) || empty($nickname)) {
            return jsonErrorReturn("paramsError", "昵称,用户名、手机号不能为空");
        }
        //当前用户已存在
        $userData = AdminUserModel::getOne(['and', ['username' => $username], ["!=", "id", $userId]]);
        if (!empty($userData)) {
            return jsonErrorReturn("fail", "用户名username唯一，相同用户名信息已存在");
        }
        $userRegData = [
            "dept_id" => loadParam("deptId", 0),
            "username" => $username,
            'nickname' => $nickname,
            'email' => loadParam("email", ""),
            "mobile" => $mobile,
            'sex' => loadParam("sex", 0),
            'status' => loadParam("status", 0),
            "update_time" => date("Y-m-d H:i:s"),
            "update_by" => $this->adminUserInfo['username'],
            "remark" => loadParam("remark", ""),
        ];
        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData, ["id" => $userId]);
        if (!empty($userRegData["id"])) {
            UserRoleModel::deleteAll(['user_id' => $userRegData["id"]]);
            $roleIds = loadParam("roleIds", []);
            if (!empty($roleIds)) {
                $batchInsert = [];
                foreach ($roleIds as $roleId) {
                    $batchInsert[] = ['role_id' => $roleId, 'user_id' => $userRegData["id"]];
                }
                if (!empty($batchInsert)) {
                    UserRoleModel::batchInsertMain($batchInsert);
                }
            }
        }

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($userRegData), '编辑成功');
    }

    //删除用户
    public function actionDeleteUser()
    {
        $userId = loadParam("userId", []);
        if (empty($userId)) {
            return jsonErrorReturn("paramsError");
        }
        AdminUserModel::updateAll(['is_delete' => 1], ["in", "id", $userId]);
        return jsonSuccessReturn([], "删除成功");
    }

    //重置密码
    public function actionResetPwd()
    {
        $userId = loadParam("userId", 0);
        $password = loadParam("password", "");
        if (empty($userId) || empty($password)) {
            return jsonErrorReturn("paramsError");
        }
        if (!RegexHelper::isPWD($password)) {
            return jsonErrorReturn("paramsError", "密码长度至少 6-18 位，必须包含有大写字母、小写字母、数字");
        }

        $userRegData = [
            "update_time" => date("Y-m-d H:i:s"),
            "update_by" => $this->adminUserInfo['username'],
            "salt" => AdminUserModel::generateSalt(),
        ];
        $userRegData["password"] = AdminUserModel::generatePassword($password, $userRegData['salt']);
        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData, ['id' => $userId]);
        return jsonSuccessReturn(["userId" => $userRegData["id"]], "重置密码成功");
    }

    //给用户分配角色
    public function actionAuthRole()
    {
        $userId = loadParam("userId", "");
        if (empty($userId)) {
            return jsonErrorReturn("paramsError");
        }
        $res = [];
        //TODO 我能分配的角色组
        $roles = AdminRoleModel::find()->alias('r')->select(['r.id as role_id', 'r.role_name', "r.role_key", "r.create_time", "r.status"])
//            ->leftJoin(UserRoleModel::tableName() . ' as ur', "ur.role_id=r.id")
//            ->where(["ur.user_id" => $this->adminUserInfo['id']])
            ->orderBy("role_sort asc")->asArray()->all();

        //用户的已有的权限
        $userRolesData = UserRoleModel::find()->select(['role_id'])->where(['user_id' => $userId])->asArray()->all();
        $userRolesArr = array_column($userRolesData, "role_id");
        foreach ($roles as &$role) {
            if (in_array($role['role_id'], $userRolesArr)) {
                $role['flag'] = true;
            } else {
                $role['flag'] = false;
            }
        }

        $res['roles'] = $roles;
        $res['user'] = AdminUserModel::getOne(['id' => $userId]);
        $res['user']['user_id'] = (int)$res['user']['id'] ?? 0;

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));
    }

    //保存用户的角色
    public function actionSaveAuthRole()
    {
        $userId = loadParam("userId");
        $roleIds = loadParam("roleIds");
        if (empty($roleIds) || empty($userId)) {
            return jsonErrorReturn("paramsError");
        }
        UserRoleModel::deleteAll(['user_id' => $userId]);
        $roleIdArr = explode(',', $roleIds);

        $batchInsert = [];
        foreach ($roleIdArr as $roleId) {
            $batchInsert[] = ['role_id' => $roleId, 'user_id' => $userId];
        }
        if (!empty($batchInsert)) {
            UserRoleModel::batchInsertMain($batchInsert);
        }

        return jsonSuccessReturn([], "操作成功");
    }

    //个人资料
    public function actionProfile()
    {
        $userId = $this->adminUserInfo['id'];
        $user = AdminUserModel::getOne(['id' => $userId]);
        $user['user_id'] = (int)$user['id'] ?? 0;
        unset($user['password']);
        unset($user['salt']);
        $res['user'] = $user;
        $res['user']['dept_name'] = AdminUserModel::getUserDept($userId)['dept_name'] ?? "";
        $res['roleList'] = AdminUserModel::getUserRole($userId);
        $res['roleGroup'] = implode(',', array_column($res['roleList'], 'role_name'));
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));
    }

    //更新个人资料
    public function actionUpdateProfile()
    {
        //当前用户已存在
        $userId = $this->adminUserInfo['id'];
        $userRegData = [
            'nickname' => loadParam("nickname", ""),
            'email' => loadParam("email", ""),
            "mobile" => loadParam("mobile", ""),
            'sex' => loadParam("sex", 0),
            "update_time" => date("Y-m-d H:i:s"),
            "update_by" => $this->adminUserInfo['username'],
        ];
        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData, ["id" => $userId]);

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($userRegData), '编辑成功');
    }

    //更新个人资料密码
    public function actionUpdateProfilePwd()
    {
        $userId = $this->adminUserInfo['id'] ?? 0;
        $oldPassword = loadParam("oldPassword", "");
        $newPassword = loadParam("newPassword", "");
        if (empty($userId) || empty($oldPassword) || empty($newPassword)) {
            return jsonErrorReturn("paramsError");
        }
        //验证旧密码
        $userData = AdminUserModel::getOne(['id' => $userId], ['id', 'salt', 'password']);
        if (empty($userData)) {
            return jsonErrorReturn("fail", "用户信息不存在");
        }
        $password = AdminUserModel::generatePassword($oldPassword, $userData['salt']);
        if ($password != $userData['password']) {
            return jsonErrorReturn("fail", "旧密码验证错误");
        }
        if (!RegexHelper::isPWD($newPassword)) {
            return jsonErrorReturn("paramsError", "密码长度至少 6-18 位，必须包含有大写字母、小写字母、数字");
        }
        //更新新密码
        $userRegData = [
            "update_time" => date("Y-m-d H:i:s"),
            "update_by" => $this->adminUserInfo['username'],
            "salt" => AdminUserModel::generateSalt(),
        ];
        $userRegData["password"] = AdminUserModel::generatePassword($newPassword, $userRegData['salt']);
        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData, ['id' => $userId]);
        return jsonSuccessReturn(["userId" => $userRegData["id"]], "重置密码成功");
    }

    //用户头像
    public function actionProfileAvatar()
    {
        try {
            $file = $_FILES['avatarfile'];
            $basePath = Yii::$app->basePath . "/web";
            $uploadDir = '/upload/' . date("Y-m") . '/' . date('d');

            if (!file_exists($uploadDir) || !is_dir($uploadDir)) {
                //"文件夹不存在";
                if (!mkdir($uploadDir, 0666, true)) {
                    return jsonErrorReturn("fail", "创建文件夹失败");
                }
            }
            //'image/png';
            $extensions = explode('/', $file['type']);
            $extension = strtolower(end($extensions));
            $fileName = ToolsHelper::getUuid() . '.' . $extension;
            //LOCAL简单上传
            $uploadBuilder = new FileUploader();
            $uploadBuilder->uploadOneFile($basePath . $uploadDir . '/' . $fileName, $file);
            //更新用户头像地址
            AdminUserModel::saveDataNoExistAdd(['avatar' => $uploadDir . '/' . $fileName], ['id' => $this->adminUserInfo['id']]);

            jsonSuccessReturn(['imgUrl' => $uploadDir . '/' . $fileName], '上传成功');
        } catch (\Exception $e) {
            jsonSuccessReturn("fail", '上传失败，' . $e->getMessage());
        }
    }

}
