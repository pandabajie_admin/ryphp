<?php

namespace backend\modules\system\controllers;

use common\extensions\helpers\TreeNewHelpers;
use common\models\AdminMenuModel;
use common\models\AdminRoleModel;
use common\models\AdminUserModel;
use common\models\RoleMenuModel;
use common\models\UserRoleModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\Debug;
use Yjius\common\ToolsHelper;

class IndexController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = ['system/index/captcha-image'];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'captcha-image' => ['GET'],
        ];
    }

    //验证码图片
    public function actionCaptchaImage()
    {
        $res['uuid'] = ToolsHelper::getUuid();
        $res['captchaEnabled'] = false;
        $res['img'] = "base64图片";
        return jsonSuccessReturn($res, "生成验证码图片成功");
    }

    //当前用户路由权限
    public function actionGetRouters()
    {
        $userMenu = AdminUserModel::getUserMenuNormal($this->adminUserInfo['id']);
        if (is_string($userMenu) && $userMenu == "*") {
            $menus = AdminMenuModel::find()->where(['status' => 0])->andWhere(['!=', 'menu_type', 'F'])->orderBy("order_num asc")->asArray()->all();
        } else {
            $menusIdArr = array_column($userMenu, 'menu_id');
            $menus = AdminMenuModel::find()->where(['status' => 0])->andWhere(['!=', 'menu_type', 'F'])->andWhere(['in', 'id', $menusIdArr])->orderBy("order_num asc")->asArray()->all();
        }
        $treeData = [];
        foreach ($menus as $menu) {
            $tmp = [];
            if ($menu['parent_id'] == '0' && $menu['is_frame'] != 0) {
                $tmp['redirect'] = "noRedirect";
                $tmp['alwaysShow'] = true;
                $tmp['path'] = '/' . $menu['path'];
                $tmp['component'] = !empty($menu['component']) ? $menu['component'] : "Layout";
            } else {
                $tmp['path'] = $menu['path'];
                if ($menu['menu_type'] == 'M') {
                    $tmp['component'] = "ParentView";
                } else {
                    $tmp['component'] = !empty($menu['component']) ? $menu['component'] : "Layout";
                }
            }
            $tmp['hidden'] = $menu['visible'] == 1 ? true : false;
            $tmp['name'] = ucfirst($menu['path']);
            $tmp['parent_id'] = $menu['parent_id'];
            $tmp['id'] = $menu['id'];
            $tmp['meta'] = [
                'icon' => $menu['icon'],
                'link' => $menu['is_frame'] == 0 ? $menu['path'] : null,
                'title' => $menu['menu_name'],
                'noCache' => $menu['is_cache'] == 1 ? true : false
            ];
            $treeData[] = $tmp;
        }
        TreeNewHelpers::instance()->init($treeData, 'parent_id');
        $treeList = TreeNewHelpers::instance()->getTreeArray(0, '', false);

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($treeList));
    }

}
