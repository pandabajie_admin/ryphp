<?php

namespace backend\modules\system\controllers;

use common\extensions\helpers\TreeNewHelpers;
use common\models\AdminMenuModel;
use common\models\RoleMenuModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\ToolsHelper;

class MenuController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = [];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'list' => ['GET'],
            'detail' => ['GET'],
            'create-menu' => ['POST'],
            'update-menu' => ['POST'],
            'delete-menu' => ['POST'],
            'treeselect' => ['GET']
        ];
    }

    //菜单列表
    public function actionList()
    {
        $menuName = loadParam("menuName");
        $status = loadParam("status");
        $query = AdminMenuModel::find();
        if (!empty($menuName)) {
            $query->andWhere(["like", "menu_name", $menuName]);
        }
        if ($status === 0 || !empty($status)) {
            $query->andWhere(["=", "status", $status]);
        }
        $menus = $query->orderBy("order_num asc")->asArray()->all();
        foreach ($menus as &$menu) {
            $menu['menu_id'] = $menu['id'];
        }
        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($menus));
    }

    //菜单详情
    public function actionDetail()
    {
        $menuId = loadParam("menuId");
        if (empty($menuId)) {
            return jsonErrorReturn("paramsError");
        }
        $menu = AdminMenuModel::getOne(["id" => $menuId]);
        $menu['menu_id'] = $menu['id'];

        $parent = AdminMenuModel::getOne(["id" => $menu['parent_id']], ['menu_name']);
        $menu['parent_name'] = $parent['menu_name'];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($menu));
    }

    //删除菜单
    public function actionDeleteMenu()
    {
        $menuId = loadParam("menuId");
        if (empty($menuId)) {
            return jsonErrorReturn("paramsError");
        }
        $children = AdminMenuModel::getOne(["parent_id" => $menuId], ['id']);
        if (!empty($children)) {
            return jsonErrorReturn("fail", "当前目录下仍有下级，无法删除。");
        }
        AdminMenuModel::deleteAll(["id" => $menuId]);
        return jsonSuccessReturn([], "删除成功");
    }


    //更新菜单
    public function actionUpdateMenu()
    {
        $menuId = loadParam("id");
        if (empty($menuId)) {
            return jsonErrorReturn("paramsError");
        }
        $menuData = [
            'menu_name' => loadParam('menuName', ''),
            'parent_id' => loadParam('parentId', 0),
            'order_num' => loadParam('orderNum', 0),
            'path' => loadParam('path', ""),
            'component' => loadParam('component', ""),
            'query' => loadParam('query', ""),
            'is_frame' => loadParam('isFrame', 0),
            'is_cache' => loadParam('isCache', 0),
            'menu_type' => loadParam('menuType', ""),
            'visible' => loadParam('visible', 0),
            'status' => loadParam('status', 0),
            'perms' => loadParam('perms', ""),
            'icon' => loadParam('icon', ""),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $updateId = AdminMenuModel::saveDataNoExistAdd($menuData, ['id' => $menuId]);
        if (!empty($updateId)) {
            return jsonSuccessReturn(['menuId' => $menuId], "编辑成功");
        } else {
            return jsonErrorReturn("fail", "编辑失败");
        }
    }

    //创建菜单
    public function actionCreateMenu()
    {
        $menuData = [
            'menu_name' => loadParam('menuName', ''),
            'parent_id' => loadParam('parentId', 0),
            'order_num' => loadParam('orderNum', 0),
            'path' => loadParam('path', ""),
            'component' => loadParam('component', ""),
            'query' => loadParam('query', ""),
            'is_frame' => loadParam('isFrame', 0),
            'is_cache' => loadParam('isCache', 0),
            'menu_type' => loadParam('menuType', ""),
            'visible' => loadParam('visible', 0),
            'status' => loadParam('status', 0),
            'perms' => loadParam('perms', ""),
            'icon' => loadParam('icon', ""),
            'remark' => loadParam('remark', ""),
            'update_time' => date("Y-m-d H:i:s"),
            'update_by' => $this->adminUserInfo['username'] ?? "",
            'create_time' => date("Y-m-d H:i:s"),
            'create_by' => $this->adminUserInfo['username'] ?? "",
        ];
        $createId = AdminMenuModel::saveDataNoExistAdd($menuData);
        if (!empty($createId)) {
            return jsonSuccessReturn(['menuId' => $createId], "新增成功");
        } else {
            return jsonErrorReturn("fail", "新增失败");
        }
    }


    //菜单树形搜索
    public function actionTreeselect()
    {
        $query = AdminMenuModel::find()->select(["id", "parent_id", "menu_name as label"]);
        $menus = $query->orderBy("order_num asc")->asArray()->all();

        TreeNewHelpers::instance()->init($menus, 'parent_id');
        $treeList = TreeNewHelpers::instance()->getTreeArray2(0, '', false);

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($treeList));

    }

    //菜单权限树形搜索
    public function actionRoleMenuTreeselect()
    {
        $roleId = loadParam("roleId");
        if (empty($roleId)) {
            return jsonErrorReturn("paramsError");
        }

        $checkedList = RoleMenuModel::find()->select(['menu_id'])->where(["role_id" => $roleId])->asArray()->all();
        $checkedKeys = array_column($checkedList,'menu_id');

        $query = AdminMenuModel::find()->select(["id", "parent_id", "menu_name as label"]);
        $menus = $query->orderBy("order_num asc")->asArray()->all();

        TreeNewHelpers::instance()->init($menus, 'parent_id');
        $treeList = TreeNewHelpers::instance()->getTreeArray2(0, '', false);

        $res = ['menus' => $treeList, 'checkedKeys' => $checkedKeys];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($res));

    }


}
