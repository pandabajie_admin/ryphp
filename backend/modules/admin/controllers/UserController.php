<?php

namespace backend\modules\admin\controllers;

use backend\services\AdminAuthService;
use common\extensions\constant\CacheConstant;
use common\models\AdminTokenModel;
use common\models\AdminUserModel;
use Yii;
use backend\extensions\BackendController;
use Yjius\common\Debug;
use Yjius\common\RegexHelper;
use Yjius\common\ToolsHelper;

class UserController extends BackendController
{

    //不需要验证登录的路由,可继承，私有
    protected $noNeedLoginRoute = ['admin/user/login', 'admin/user/register', 'admin/user/logout'];


    //接口请求类型限制
    protected function verbs()
    {
        return [
            'login' => ['POST'],
            'logout' => ['POST'],
            'register' => ['POST'],
        ];
    }

    //登录方法
    public function actionLogin()
    {
        $username = loadParam("username", "");
        $password = loadParam("password", "");
        if (empty($username) || empty($password)) {
            return jsonErrorReturn("paramsError", "用户名或密码不能为空");
        }
        if (!RegexHelper::isPWD($password)) {
            return jsonErrorReturn("paramsError", "密码长度至少 6-18 位，必须包含有大写字母、小写字母、数字");
        }
        // ... 进行用户名密码验证
        $userData = AdminUserModel::getOne(['username' => $username], ['id', 'salt', 'password', 'login_time', 'nickname', 'username', 'mobile', 'create_time']);
        if (empty($userData)) {
            return jsonErrorReturn("fail", "用户信息不存在");
        }
        $password = AdminUserModel::generatePassword($password, $userData['salt']);

        if ($password != $userData['password']) {
            return jsonErrorReturn("fail", "用户名或密码错误");
        }
        // ... 验证通过后，生成token，并返回给客户端

        $jwt = AdminAuthService::generateToken($userData);

        $data = ["token" => $jwt];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($data), '登录成功');
    }

    //退出登录
    public function actionLogout()
    {
        //删除缓存
        Yii::$app->cache->delete(CacheConstant::ADMIN_USER_TOKEN . $this->jwt);
        //删除数据库
//        AdminTokenModel::deleteAll(['user_id' => $this->adminUserInfo['id']]);
        return jsonSuccessReturn([], '退出成功');
    }


    //注册用户
    public function actionRegister()
    {
        return jsonErrorReturn("fail", "后台项目暂时不支持注册");

        $username = loadParam("username", "");
        $mobile = loadParam("mobile", "");
        $password = loadParam("password", "");
        if (empty($mobile) || empty($username) || empty($password)) {
            return jsonErrorReturn("paramsError", "用户名、手机号、密码不能为空");
        }
        if (!RegexHelper::isPWD($password)) {
            return jsonErrorReturn("paramsError", "密码长度至少 6-18 位，必须包含有大写字母、小写字母、数字");
        }
        //当前用户已存在
        $userData = AdminUserModel::getOne(['username' => $username]);
        if (!empty($userData)) {
            return jsonErrorReturn("fail", "相同用户名信息已存在");
        }
        $userRegData = [
            "username" => $username,
            "create_time" => date("Y-m-d H:i:s"),
            "login_time" => date("Y-m-d H:i:s"),
            "mobile" => $mobile,
            "salt" => AdminUserModel::generateSalt(),
            'nickname' => $username
        ];
        $userRegData["password"] = AdminUserModel::generatePassword($password, $userRegData['salt']);

        $userRegData["id"] = AdminUserModel::saveDataNoExistAdd($userRegData);

        $jwt = AdminAuthService::generateToken($userRegData);

        $data = ["token" => $jwt];

        return jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($data), '注册成功');

    }
}
