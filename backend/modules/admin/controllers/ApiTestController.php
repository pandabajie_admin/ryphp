<?php

namespace backend\modules\admin\controllers;

use backend\extensions\BackendController;
use Yii;
use Yjius\common\ToolsHelper;

class ApiTestController extends BackendController
{
    protected $noNeedLoginRoute = ['admin/api-test/not-token'];

    //需要登录接口
    public function actionInfo()
    {
        jsonSuccessReturn(ToolsHelper::humpUnderlineConversion($this->adminUserInfo));
    }

    //不需要登录接口
    public function actionNotToken()
    {
        jsonSuccessReturn([], "当前接口成功返回、无需认证登录");
    }

}