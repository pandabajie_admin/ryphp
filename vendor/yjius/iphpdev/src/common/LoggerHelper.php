<?php

namespace Yjius\common;

/**
 * 调试日志操作类
 */
class LoggerHelper
{

    private static $instance = false;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * 初始化调试日志操作类，没有经过初始化的后续调试代码都不会生效
     */
    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new LoggerHelper();
        }
        return self::$instance;
    }

    /**
     * 写入日志文件
     * @param string $file 文件名
     * @param string $msg 日志内容
     * @param boolean $time 是否记录当前时间
     * @param boolean $ip 是否记录访问者IP
     */
    public static function write($file, $msg, $time = true, $ip = true)
    {
        $info = [];
        $time && $info["time"] = date('Y-m-d H:i:s');
        if ($ip) {
            //部分情况无法获得IP地址，比如用命令行模式操作
            try {
                $userIP = ToolsHelper::getIpAddr();
            } catch (\Exception $e) {
                $userIP = 'UNKNOW|CONSOLE';
            }
            $info["ip"] = $userIP;
        }
        $info["msg"] = $msg;
        if (!defined('LOG_PATH')) {
            define('LOG_PATH', dirname(dirname(__DIR__)) . '/examples/runtime/log');
        }
        $dir_path = LOG_PATH . '/' . date('Y-m') . '/' . date('d') . '/';
        if (!is_dir($dir_path)) {
            @mkdir($dir_path, 0777, true);
            @chmod($dir_path, 0777);
        }
        if (!file_exists($dir_path . $file)) {
            $flag = 1;//新增的话去修改文件权限
        }
        $infoJson = json_encode($info, JSON_UNESCAPED_UNICODE);
        file_put_contents($dir_path . $file, $infoJson . PHP_EOL, FILE_APPEND);
        if (!empty($flag)) {
            @chmod($dir_path . $file, 0777);
        }
    }
}

