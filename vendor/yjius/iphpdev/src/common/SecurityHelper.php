<?php
namespace Yjius\common;

class SecurityHelper
{

    /**
     * 加密字符串
     *
     * @param $string
     * @return string
     */
    public static function Yencrypt($string)
    {
        $keys = array('CI9SsW', 'bxlYmF');
        $agent = substr(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ROBOTS', -5, 5);
        $vala = 0;

        for ($i = 0; $i < strlen($agent); $i++) {
            $vala += ord($agent{$i});
        }

        $vala = substr(strval($vala), -1, 1);
        $valb = (string)random_int(1000, 9999);
        $flag = (strlen($string) % 2) == 1 ? 1 : 0;
        $tmp = $valb . $keys[$flag] . base64_encode($string) . $keys[$flag ^ 1];
        $tmp2 = '';

        for ($i = 0; $i < strlen($tmp) * 2; $i++) {
            if ($i % 2 == 0) {
                $tmp2 .= $valb{$i / 2 % 4};
            } else {
                $tmp2 .= $tmp{($i - 1) / 2};
            }
        }

        $tmp = $valb . base64_encode(strrev($tmp2)) . $flag . $vala;

        return $tmp;

    }

    /**
     * 解密字符串
     *
     * @param $string
     * @return bool|string
     */
    public static function Ydecrypt($string)
    {
        $keys = array('CI9SsW', 'bxlYmF');
        $agent = substr(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ROBOTS', -5, 5);
        $vala = 0;

        for ($i = 0; $i < strlen($agent); $i++) {
            $vala += ord($agent{$i});
        }

        $vala = substr(strval($vala), -1, 1);
        $valb = substr($string, 0, 4);

        if (substr($string, -1, 1) != $vala) {
            return FALSE;
        }

        $flag = intval(substr($string, -2, 1));
        $tmp = base64_decode(substr($string, 4, strlen($string) - 6));
        $tmp = strrev($tmp);
        $tmp2 = '';

        for ($i = 0; $i < strlen($tmp); $i++) {
            if ($i % 2 == 1) {
                $tmp2 .= $tmp{$i};
            }
        }

        if (
            substr($tmp2, 4, 6) != $keys[$flag]
            || substr($tmp2, -6, 6) != $keys[$flag ^ 1]
            || substr($tmp2, 0, 4) != $valb
        ) {
            return FALSE;
        }

        $tmp2 = base64_decode(substr($tmp2, 10, strlen($tmp2) - 16));

        return $tmp2;
    }

}
