<?php

namespace Yjius\common;

class FolderHelper
{
    /**
     * 设置需要删除的文件夹,清空文件夹函数和清空文件夹后删除空文件夹函数的处理
     * rmDir是系统方法只能删除空文件夹
     * 此处是循环删除子文件夹
     * Purpose: delDir
     * Author: Jius Ying
     * @date : 2023/6/15
     * @param $path
     */
    public function delDir($path)
    {
        //如果是目录则继续
        if (is_dir($path)) {
            //扫描一个文件夹内的所有文件夹和文件并返回数组
            $p = scandir($path);
            foreach ($p as $val) {
                //排除目录中的.和..
                if ($val != "." && $val != "..") {
                    //如果是目录则递归子目录，继续操作
                    if (is_dir($path . $val)) {
                        //子目录中操作删除文件夹和文件
                        $this->delDir($path . $val . '/');
                        //目录清空后删除空文件夹
                        @rmdir($path . $val . '/');
                    } else {
                        //如果是文件直接删除
                        unlink($path . $val);
                    }
                }
            }
        }
    }
}