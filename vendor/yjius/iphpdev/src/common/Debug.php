<?php

namespace Yjius\common;

/**
 * 调试-打印
 * 可打印多个值或内容
 * Class DebugLog
 */
defined('YII_DEBUG') or define('YII_DEBUG', true);

class Debug
{
    public static function __callStatic($func, $arguments)
    {
        if (YII_DEBUG) {
            if ($func == 'dump') {
                var_dump($arguments[0]);
                die();
            } else if (function_exists($func)) {
                echo '<pre>';
                $func($arguments);
                echo '<br>';
                echo '===============================⏫⏫⏫⏫===============================<br>';
                $info = debug_backtrace();
                if (isset($info[1])) {
                    print_r('Class：' . $info[1]['class'] . '<br>');
                    print_r('Type：' . $info[1]['type'] . '<br>');
                    print_r('Function：' . $info[1]['function'] . '<br>');
                    echo 'Args：';
                    print_r($info[1]['args']);
                    echo '<br>';
                }
                print_r('Line：' . $info[0]['line'] . '<br>');
                print_r('File：' . $info[0]['file'] . '<br>');
                unset($info);

                die();
            } else {
                echo $func . ' function is not exist!';
                die();
            }
        }
    }
}