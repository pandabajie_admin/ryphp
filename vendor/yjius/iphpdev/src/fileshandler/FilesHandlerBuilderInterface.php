<?php

namespace Yjius\fileshandler;

interface FilesHandlerBuilderInterface
{
    /**
     * PHP 上传文件方法，适用于PHP后端小文件上传
     * $fileObject  oss上存的文件名地址 或 local存储的地址路径
     * $localFile  oss本地文件或临时文件的地址 或 local:$localFile = $_FILE['file']
     */
    function uploadOneFile($fileObject, $localFile, $options = null);

    /**
     * Purpose: 批量删除文件
     * @date 2021/11/24 15:51
     */
    function deleteFiles($files = []);

    /**
     * Purpose: OSS前端验证
     */
    public function getPolicy();
    /**
     * Purpose: OSS临时访问地址
     */
    public function getSignUrl($object, $timeout = 3600);
    /**
     * Purpose: 本地上传方法三连
     */
    public function checkFile($fileInfo, $tmpDir = '', $saveDir = '');
    public function upload($fileInfo, $tmpDir = '', $saveDir = '');
    public function merge($fileInfo, $tmpDir = '', $saveDir = '');
    /**
     * Purpose: OSS范围下载
     */
    public function getRangeObject($object, $range = '0-10');

}