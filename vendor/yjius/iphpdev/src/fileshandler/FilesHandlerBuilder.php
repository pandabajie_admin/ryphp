<?php

namespace Yjius\fileshandler;

class FilesHandlerBuilder implements FilesHandlerBuilderInterface
{
    private $instance = null;

    /*
    // 用于oss上传配置
    'ossParams' => [
    'driver' => 'oss',
    'accessKeyId' => 'LTAI5t757vyeaJJYtZueGGEs',
    'accessKeySecret' => 'mw4paZ9Pkr2HB4wLJDqsDXct2Pl0ng',
    'endpoint' => 'oss-cn-hangzhou.aliyuncs.com',
    'bucket' => 'bsddata',
    'storePath' => 'teaching_dev',//存储的文件夹地址前缀
    ],
    //用于本地上传配置
    'localParams' => [
    'driver' => 'local',
    ],
    */

    public function __construct($params = [])
    {
        switch ($params['driver']){
            case 'local':
            case 'LOCAL':
                $this->instance = local\LocalBuilder::getInstance($params);
                break;
            case 'oss':
            case 'OSS':
                $this->instance = oss\OssBuilder::getInstance($params);
                break;
            default:
                throw new \Exception("暂不支持其他类型调用");
        }
    }

    /**
     * PHP上传单个文件
     * $fileObject  oss上存的文件名地址 local上传存储路径
     * $localFilePath  oss上传本地文件存储地址 local本地文件或临时文件$FILE内容
     */
    public function uploadOneFile($fileObject, $localFile, $options = NULL)
    {
        return $this->instance->uploadOneFile($fileObject, $localFile, $options = NULL);
    }

    /**
     * 批量删除文件
     * $filesObject 文件路径数组 ossobject数组  local文件存储路径
     */
    public function deleteFiles($filesObject = [])
    {
        return $this->instance->deleteFiles($filesObject);
    }

    /**
     * Purpose: OSS 获取前端签名 LOCAL不支持
     * @date 2021/11/24 17:41
     */
    public function getPolicy(){
        return $this->instance->getPolicy();
    }

    /**
     * Purpose: OSS 临时访问链接示例 LOCAL不支持
     */
    public function getSignUrl($object, $timeout = 3600){
        return $this->instance->getSignUrl($object, $timeout);
    }

    /**
     * Purpose: 本地大文件上传方法三连
     * @date 2021/11/25 11:30
     */
    public function checkFile($fileInfo, $tmpDir = '', $saveDir = '')
    {
        return $this->instance->checkFile($fileInfo, $tmpDir, $saveDir);
    }

    /**
     * Purpose: 本地大文件上传方法三连
     * @date 2021/11/25 11:30
     */
    public function upload($fileInfo, $tmpDir = '', $saveDir = '')
    {
        return $this->instance->upload($fileInfo, $tmpDir, $saveDir);
    }

    /**
     * Purpose: 本地大文件上传方法三连
     * @date 2021/11/25 11:30
     */
    public function merge($fileInfo, $tmpDir = '', $saveDir = '')
    {
        return $this->instance->merge($fileInfo, $tmpDir, $saveDir);
    }

    /**
     * Purpose: OSS 范围下载
     */
    public function getRangeObject($object, $range = '0-10'){
        return $this->instance->getRangeObject($object, $range);
    }

}