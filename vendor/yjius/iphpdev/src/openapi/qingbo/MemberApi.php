<?php

namespace Yjius\openapi\qingbo;
use Yjius\common\CurlHelper;

/**
 * 清博
 * 指数账号中心接口封装
 * Class MemberApi
 * @package Yjiusopenapi
 * @date  2022/3/17 16:42
 */
class MemberApi
{

    protected $host = "https://member.gsdata.cn";
    protected $token = null;
    protected $key = 'b3!jX.Ng75=An4y['; //解析key
    protected $cookieName = "_gsdataCL"; //解析key

    public function __construct($config = [])
    {
        if (!empty($config['token'])) {
            $this->token = $config['token'];
        }
        if (!empty($config['host'])) {
            $this->host = $config['host'];
        }
        if (!empty($config['key'])) {
            $this->key = $config['key'];
        }
        if (!empty($config['cookieName'])) {
            $this->cookieName = $config['cookieName'];
        }
        if (empty($this->token)) {
            throw new \Exception("请联系相关人员获取合法的token");
        }
    }

    /**
     * Purpose: 解析账号中心-cookie是否合法
     * @date 2021/12/3 17:49
     */
    public function explainCookie($cookieStr)
    {
        if (empty($cookieStr)) {
            $cookieStr = isset($_COOKIE[$this->cookieName]) ? $_COOKIE[$this->cookieName] : '';
        }
        if (empty($cookieStr)) {
            throw new \Exception("请传入正确的参数或cookie不存在");
        }
        $params = json_decode(base64_decode($cookieStr), true);
        $uid = @$params[0];
        $name = @$params[1];
        $timestamp = @$params[2];
        $sign = @$params[3];
        $member_id = @$params[4];

        if ($this->signature($uid . $name . $timestamp . $member_id) == $sign) {
            return ['member_id' => $member_id];
        }
        throw new \Exception('不合法的cookie内容，无法解析');
    }

    /**
     * Purpose: 查询用户是否禁用
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/17 17:52
     */
    public function forbiddenInfo($memberId)
    {
        if (empty($memberId)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/forbidden_info';
        $params = [
            'member_id' => $memberId,
        ];
        return $this->call($url, $params);
    }


    /**
     * 用户注册
     * @param string $nickname 昵称
     * @param string $mobile 手机号
     * @param string $password 密码
     */
    public function register($nickname, $mobile, $password)
    {
        if (empty($nickname) || empty($nickname) || empty($password)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/register';
        $params = [
            'username' => $mobile,
            'mobile' => $mobile,
            'password' => $password,
            'nickname' => $nickname
        ];
        return $this->call($url, $params);
    }

    /**
     * 用户登录校验
     * @param string $mobile 用户手机号码
     * @param string $password 用户密码
     * @param string $ip 用户IP地址
     */
    public function login($mobile, $password, $ip = '')
    {
        if (empty($mobile) || empty($password)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/login';
        $params = [
            'mobile' => $mobile,
            'password' => $password,
            'ip' => $ip,
        ];
        return $this->call($url, $params);
    }

    /**
     * Purpose: 用户ID查询用户信息
     * @param string $username
     */
    public function getUserInfoById($memberId)
    {
        if (empty($memberId)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/info';
        $params = [
            'id' => $memberId
        ];
        return $this->call($url, $params);
    }

    /**
     * Purpose: 用户名查询用户信息
     * @param string $username
     */
    public function getUserInfoByUsername($username)
    {
        if (empty($username)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/info';
        $params = [
            'username' => $username
        ];
        return $this->call($url, $params);
    }

    /**
     * Purpose: 手机号查询用户信息
     * @date 2021/12/3 17:33
     */
    public function getUserInfoByMobile($mobile)
    {
        if (empty($mobile)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/info';
        $params = [
            'mobile' => $mobile
        ];
        return $this->call($url, $params);
    }

    /**
     * Purpose: 修改密码
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/12/3 17:36
     */
    public function editpassword($mobile, $password)
    {
        if (empty($mobile) || empty($password)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/member/modify';
        $params = [
            'mobile' => $mobile,
            'password' => $password,
        ];
        return $this->call($url, $params);
    }

    /**
     * 账号中心-发送短信
     * ('186****0579','【测试】测试短信内容')
     */
    public function sendSms($mobile, $content)
    {
        if (empty($mobile) || empty($content)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/common/sendztsms';
        $params['mobile'] = $mobile;
        $params['content'] = $content;
        return $this->call($url, $params);
    }

    /**
     * Purpose: 账号中心-发送邮件通知
     * @date 2021/12/3 17:28
     */
    public function sendEmail($to, $subject, $body)
    {
        if (empty($to) || empty($subject) || empty($body)) {
            throw new \Exception("请传入正确的参数");
        }
        $url = '/api/v1/common/sendemail';
        $params = [
            'from' => 'noreply',
            'to' => $to,
            'subject' => $subject,
            'body' => $body
        ];
        return $this->call($url, $params);
    }

    /**
     * 请求接口
     * @param string $path 接口相对路径如：/api/v1/member/info
     * @param array $params 提交参数
     * @return //接口返回数据，已转为数组
     */
    private function call($path, $params = [])
    {
        $params['token'] = $this->token;
        $res = CurlHelper::httpRequest($this->host . $path, [], "POST", null, $params);
        $return = json_decode($res, true);
        if (!isset($return['errcode'])) {
            throw new \Exception('服务器繁忙，请稍后刷新重试');
        }
        return $return;
    }

    /**
     * 签名
     */
    private function signature($str)
    {
        return md5(strtolower($str) . $this->key);
    }
}
