<?php

namespace Yjius\openapi\qingbo;

use Yjius\common\CurlHelper;

/**
 * 清博热点
 * 接口文档地址
 * https://apihot.gsdata.cn/swagger-ui/dist/index.html
 * Class HotApi
 * @package Yjiusopenapi
 * @date  2022/3/4 11:15
 */
class HotApi
{
    protected $api_host = 'http://apihot.gsdata.cn';
    protected $api_secret = "";

    public function __construct($config = [])
    {
        if (!empty($config['api_host'])) {
            $this->api_host = $config['api_host'];
        }
        if (!empty($config['api_secret'])) {
            $this->api_secret = $config['api_secret'];
        }
        if (empty($this->api_secret)) {
            throw new \Exception("请联系相关人员获取合法的api_secret");
        }
    }

    /**
     * Purpose: 首页7日热点地图数据
     * /news/index/hot-list-map
     * type      ：      榜单类型:1.当日; 2.昨日; 3.近三日; 7.近七日
     * news_type ：      新闻类别:1默认,2微信,3微博,4综合,5头条,6抖音, 默认是1
     * tag       ：      标签，为空时不传
     * date      ：      查询单日日期时用，不传则为当日
     * province  ：      省份
     * city      ：      城市
     *
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/4 15:10
     */
    public function hotListMap($params)
    {
        $request_url = $this->api_host . "/news/index/hot-list-map";
        $params["api_secret"] = $this->api_secret;
        $params["type"] = isset($params["type"]) ? $params["type"] : 7;
        $params["news_type"] = isset($params["news_type"]) ? $params["news_type"] : 4;
        $params["province"] = isset($params["province"]) ? $params["province"] : '';
        $params["city"] = isset($params["city"]) ? $params["city"] : '';
        if (empty($params["tag"])) {
            unset($params["tag"]);
        }
        if (empty($params["date"])) {
            unset($params["date"]);
        }
        $res = CurlHelper::httpRequest($request_url, [], "GET", $params);
        $res = json_decode($res, true);
        if (isset($res["code"]) && $res["code"] == 10000) {
            return $res["data"];
        } else {
            return $res["msg"];
        }
    }


    /**
     * Purpose: 首页7日热点列表数据
     * /news/index/hot-list
     * type      ：      榜单类型:1.当日; 2.昨日; 3.近三日; 7.近七日
     * news_type ：      新闻类别:1默认,2微信,3微博,4综合,5头条,6抖音, 默认是1
     * tag       ：      标签，为空时不传
     * date      ：      查询单日日期时用，不传则为当日
     * province  ：      省份
     * city      ：      城市
     * county    ：      区域
     * page      ：      页码
     * perpage   ：      页数
     *
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/4 15:10
     */
    public function hotRankList($params)
    {
        $request_url = $this->api_host . "/news/index/hot-list";
        $params["api_secret"] = $this->api_secret;
        $params["type"] = isset($params["type"]) ? $params["type"] : 7;
        $params["news_type"] = isset($params["news_type"]) ? $params["news_type"] : 4;
        $params["province"] = isset($params["province"]) ? $params["province"] : '';
        $params["city"] = isset($params["city"]) ? $params["city"] : '';
        $params["county"] = isset($params["county"]) ? $params["county"] : '';
        if (empty($params["tag"])) {
            unset($params["tag"]);
        }
        if (empty($params["date"])) {
            unset($params["date"]);
        }
        $params["page"] = isset($params["page"]) ? $params["page"] : 1;
        $params["perpage"] = isset($params["perpage"]) ? $params["perpage"] : 10;
        $res = CurlHelper::httpRequest($request_url, [], "GET", $params);
        $res = json_decode($res, true);
        if (isset($res["code"]) && $res["code"] == 10000) {
            return $res["data"];
        } else {
            return $res["msg"];
        }
    }


    /**
     * Purpose: 晨晚报列表数据
     * /news/index/hot-list-newspaper
     * time      ：      查询单日日期时用，不传则为当日
     * type      ：      1早报 2晚报
     * tag       ：      标签，为空时不传
     * province  ：      省份
     * city      ：      城市
     * county    ：      区域
     * page      ：      页码
     * perpage   ：      页数
     *
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/4 15:10
     */
    public function hotListNewspaper($params)
    {
        $request_url = $this->api_host . "/news/index/hot-list-newspaper";
        $params["api_secret"] = $this->api_secret;

        $params["time"] = isset($params["time"]) ? $params["time"] : date('Y-m-d');
        $params["type"] = isset($params["type"]) ? $params["type"] : 1;
        if (empty($params["tag"])) {
            unset($params["tag"]);
        }
        $params["province"] = isset($params["province"]) ? $params["province"] : '';
        $params["city"] = isset($params["city"]) ? $params["city"] : '';
        $params["county"] = isset($params["county"]) ? $params["county"] : '';
        $params["page"] = isset($params["page"]) ? $params["page"] : 1;
        $params["perpage"] = isset($params["perpage"]) ? $params["perpage"] : 10;

        $res = CurlHelper::httpRequest($request_url, [], "GET", $params);
        $res = json_decode($res, true);
        if (isset($res["code"]) && $res["code"] == 10000) {
            return $res["data"];
        } else {
            return $res["msg"];
        }
    }

    /**
     * Purpose: 读数据接口
     * /news/index/index
     * news_type    ：   新闻类型,1网页,2微信,3微博,4综合,5头条,6抖音;默认是4
     * search       ：   搜索关键词
     * order_type   ：   排序:time_down时间降序;hot_down热度降序;默认时间降序
     * tag          ：   标签，为空时不传
     * mood         ：   情绪：好/惊/怒....
     * news_emotion ：   情感:正面,负面,中性
     * province     ：   省份
     * city         ：   城市
     * county       ：   区域
     * start_time   ：   开始时间
     * end_time     ：   结束时间
     * page         ：   页码
     * perpage      ：   页数
     *
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/4 17:35
     */
    public function hotList($params)
    {
        $request_url = $this->api_host . "/news/index/index";
        $params["api_secret"] = $this->api_secret;
        $params["news_type"] = isset($params["news_type"]) ? $params["news_type"] : 4;
        $params["search"] = isset($params["search"]) ? $params["search"] : '';
        $params["order_type"] = isset($params["order_type"]) ? $params["order_type"] : 'time_down';
        if (empty($params["tag"])) {
            unset($params["tag"]);
        }
        if (empty($params["mood"])) {
            unset($params["mood"]);
        }
        if (empty($params["news_emotion"])) {
            unset($params["news_emotion"]);
        }
        $params["start_time"] = isset($params["start_time"]) ? $params["start_time"] : date('Y-m-d 00:00:00');
        $params["end_time"] = isset($params["end_time"]) ? $params["end_time"] : date('Y-m-d 23:59:59');
        $params["province"] = isset($params["province"]) ? $params["province"] : '';
        $params["city"] = isset($params["city"]) ? $params["city"] : '';
        $params["county"] = isset($params["county"]) ? $params["county"] : '';
        $params["page"] = isset($params["page"]) ? $params["page"] : 1;
        $params["perpage"] = isset($params["perpage"]) ? $params["perpage"] : 10;
        $res = CurlHelper::httpRequest($request_url, [], "GET", $params);
        $res = json_decode($res, true);
        if (isset($res["code"]) && $res["code"] == 10000) {
            return $res["data"];
        } else {
            return $res["msg"];
        }
    }


}