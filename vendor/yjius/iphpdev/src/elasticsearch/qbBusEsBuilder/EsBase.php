<?php

namespace Yjius\elasticsearch\qbBusEsBuilder;

use Yjius\common\CurlHelper;
use Yjius\elasticsearch\jfxyElasticsearchQuery\Builder;

abstract class EsBase extends Builder
{
    //ES的token认证
    private $esToken = "";
//    //请求es网关地址
    private $esGatewayUrl = "";
//    //请求es网关地址
    private $esScrollGatewayUrl = "";

    //默认使用全局开始时间
    private $minStartTime = null;
    //默认使用全局结束时间
    private $maxEndTime = null;
    //默认使用的全局索引
    private $originIndex = "";
    //传入开始时间
    private $startTime = null;
    //传入结束时间
    private $endTime = null;
    //传入索引
    private $index = "";
    //最终返回的字段 默认查询的字段
    public $fields = [];


    /**
     * 可以对外调用
     * 使用select时此处应该无需使用
     * 新增查询字段
     * @param $fields
     * @return EsBase
     */
    public function appendFields($fields): self
    {
        $fields = is_array($fields) ? $fields : func_get_args();

        $this->fields = array_values(array_unique(array_merge(self::fields(), $fields)));

        return $this;
    }

    //设置一个最大最小的查询区间，如果有传入时间取传入时间
    public function getTime($type = 'start', $stamp = false)
    {
        if ($type == 'start') {
            $time = empty($this->startTime) ? $this->minStartTime : $this->startTime;
        } else {
            $time = empty($this->endTime) ? $this->maxEndTime : $this->endTime;
        }
        if ($stamp) $time = strtotime($time);
        return $time;
    }

    //设置dsl去查询
    public function query()
    {
        if (!is_string($this->dsl)) {
            $this->dsl = json_encode($this->dsl, JSON_UNESCAPED_UNICODE);
        }
        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization:' . $this->getEsToken();
        $headers[] = 'Expect:';

        $startTime = $this->getTime('start', true) . '000';
        $endTime = $this->getTime('end', true) . '000';

        $this->index = !empty($this->index) ? $this->index : $this->originIndex;

        $params = [
            'index' => $this->index,
            'statement' => $this->dsl,
            'startStamp' => $startTime,
            'endStamp' => $endTime
        ];
        $params = json_encode($params);

        return CurlHelper::httpRequest($this->getEsGatewayUrl(), $headers, "POST", null, $params);
    }

    //游标查询
    public function scrollQuery()
    {
        if (!is_string($this->dsl)) {
            $this->dsl = json_encode($this->dsl, JSON_UNESCAPED_UNICODE);
        }
        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization:' . $this->getEsToken();
        $headers[] = 'Expect:';

        $startTime = $this->getTime('start', true) . '000';
        $endTime = $this->getTime('end', true) . '000';

        $this->index = !empty($this->index) ? $this->index : $this->originIndex;

        $params = [
            'index' => $this->index,
            'statement' => $this->dsl,
            'startStamp' => $startTime,
            'endStamp' => $endTime,
            'scrollId' => $this->scrollId
        ];
        $params = json_encode($params);

        return CurlHelper::httpRequest($this->getEsScrollGatewayUrl(), $headers, "POST", null, $params);
    }


    /**
     * 没有select 可以通过此处过滤只需要查询的字段
     * 相当于默认的select作用
     * 使用select时此处应该无需使用
     * 基于新增字段时，默认返回字段（待补充）
     * @return array
     */
    public static function fields()
    {
        return [];
    }


    /**
     * @return string
     */
    public function getEsGatewayUrl(): string
    {
        return $this->esGatewayUrl;
    }

    /**
     * @param string $esGatewayUrl
     */
    public function setEsGatewayUrl(string $esGatewayUrl)
    {
        $this->esGatewayUrl = $esGatewayUrl;
    }

    /**
     * @return string
     */
    public function getEsScrollGatewayUrl(): string
    {
        return $this->esScrollGatewayUrl;
    }

    /**
     * @param string $esScrollGatewayUrl
     */
    public function setEsScrollGatewayUrl(string $esScrollGatewayUrl)
    {
        $this->esScrollGatewayUrl = $esScrollGatewayUrl;
    }

    /**
     * @return string
     */
    public function getEsToken(): string
    {
        return $this->esToken;
    }

    /**
     * @param string $esToken
     */
    public function setEsToken(string $esToken)
    {
        $this->esToken = $esToken;
    }


    /**
     * @return null
     */
    public function getMinStartTime()
    {
        return $this->minStartTime;
    }

    /**
     * @param null $minStartTime
     */
    public function setMinStartTime($minStartTime)
    {
        $this->minStartTime = $minStartTime;
    }

    /**
     * @return null
     */
    public function getMaxEndTime()
    {
        return $this->maxEndTime;
    }

    /**
     * @param null $maxEndTime
     */
    public function setMaxEndTime($maxEndTime)
    {
        $this->maxEndTime = $maxEndTime;
    }

    /**
     * @return string
     */
    public function getOriginIndex(): string
    {
        return $this->originIndex;
    }

    /**
     * @param string $originIndex
     */
    public function setOriginIndex(string $originIndex)
    {
        $this->originIndex = $originIndex;
    }

    /**
     * @return null
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param null $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return null
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param null $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @param string $index
     */
    public function setIndex(string $index)
    {
        $this->index = $index;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }

}