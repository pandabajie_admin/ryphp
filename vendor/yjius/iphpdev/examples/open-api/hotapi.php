<?php
use Yjius\common\Debug;
use Yjius\openapi\qingbo\HotApi;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $config = require dirname(__DIR__) . "/config/open-api/hotapi.php";
    $hotapiService = new HotApi($config);

//    首页7日热点地图数据
    $params = ['province' => "北京市", 'type' => 7];
    $res1 = $hotapiService->hotListMap($params);
//    首页7日榜单列表数据
    $params = ['type' => 3];
    $res2 = $hotapiService->hotRankList($params);
//    热点晨晚报
    $params = ["type" => 2, "time" => '2022-03-03'];
    $res3 = $hotapiService->hotListNewspaper($params);
    //    读数据列表
    $params = [];
    $res4 = $hotapiService->hotList($params);

    Debug::print_r($res4);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}