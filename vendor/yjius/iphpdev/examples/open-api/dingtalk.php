<?php
use Yjius\common\Debug;
use Yjius\openapi\HotApi;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子 -已通
    $config = require dirname(__DIR__) . "/config/open-api/dingtalk.php";
    $d2 = new \Yjius\openapi\dingtalk\DingTalkOAuth($config['pm_test']);
    $res = $d2->auth('http://ding');
    Debug::print_r($res);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}