<?php
use Yjius\common\Debug;
use Yjius\fileshandler\FilesHandlerBuilder;

require __DIR__ . "/../../vendor/autoload.php";

$ossParams = require dirname(__DIR__) . "/config/files-handler/oss.php";
$localParams = require dirname(__DIR__) . "/config/files-handler/local.php";
try {

    //OSS 提供验签，用于大文件web直传到oss
    $uploadBuilder = new FilesHandlerBuilder($ossParams);
    $re = $uploadBuilder->getPolicy();

    //OSS 临时访问链接示例
    $uploadBuilder = new FilesHandlerBuilder($ossParams);
    $re1 = $uploadBuilder->getSignUrl('teaching_dev/test/fruit1.png');

    //OSS 范围下载
    $uploadBuilder = new FilesHandlerBuilder($ossParams);
    $re2 = $uploadBuilder->getRangeObject('teaching_dev/test/fruit1.png');
    Debug::print_r($re, $re1, $re2);

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
