<?php
use Yjius\fileshandler\FilesHandlerBuilder;

require __DIR__ . "/../../vendor/autoload.php";

$ossParams = require dirname(__DIR__) . "/config/files-handler/oss.php";
$localParams = require dirname(__DIR__) . "/config/files-handler/local.php";
try {

//    deleteFiles
//    oss    要删除的文件路径地址数组
//    local  要删除的文件路径地址数组

    $file = $_FILES['file'];
    //LOCAL简单上传
    $uploadBuilder = new FilesHandlerBuilder($localParams);
    $re = $uploadBuilder->deleteFiles([dirname(dirname(__FILE__)) . '/runtime/tmp/test_local.png', dirname(dirname(__FILE__)) . '/runtime/tmp/fruit_local.png']);
    print_r($re);
    print_r('<br/>');

    //OSS 简单上传
    $uploadBuilder = new FilesHandlerBuilder($ossParams);
    $uploadBuilder->deleteFiles(['teaching_dev/test/fruit_local.png', 'teaching_dev/test/test_local.png']);
    print_r($re);

    exit;

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
