<?php
use Yjius\common\Debug;
use Yjius\common\SecurityHelper;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //加密方法
    $str = SecurityHelper::Yencrypt("这是加密内容");
    //解密方法
    $res = SecurityHelper::Ydecrypt($str);
    Debug::print_r($str, $res);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}