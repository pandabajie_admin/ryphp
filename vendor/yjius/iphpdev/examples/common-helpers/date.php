<?php
use Yjius\common\Debug;
use Yjius\common\DateHelper;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子

    //时区时间差，秒数计算
    //芝加哥 比 世界时间慢  21600秒
    $res1 = DateHelper::offset('America/Chicago', 'GMT');
    //上海时间 比 世界时间快  28800秒
    $res2 = DateHelper::offset('Asia/Shanghai', 'GMT');
    //上海时间 比 当前运行环境时间差相等差  0秒
    $res3 = DateHelper::offset('Asia/Shanghai');

    //计算两个时间戳相差的时分秒
    //Array([days] => 2,[hours] => 1,[minutes] => 1,[seconds] => 1 )
    $res4 = DateHelper::span(strtotime('2022-02-24 23:23:23'), strtotime('2022-02-22 22:22:22'), 'days,hours,minutes,seconds');
    //Array([hours] => 49,[minutes] => 1,[seconds] => 1 )
    $res5 = DateHelper::span(strtotime('2022-02-24 23:23:23'), strtotime('2022-02-22 22:22:22'), 'hours,minutes,seconds');
    //176461
    $res5 = DateHelper::span(strtotime('2022-02-24 23:23:23'), strtotime('2022-02-22 22:22:22'), 'seconds');

    //获取时间戳偏移量-当前时间的两个小时后的开始时间
    //2022-02-24 13:00:00
    $res6 = DateHelper::unixtime('hour', 2, 'begin');
    //获取时间戳偏移量-当前时间的两个小时后的结束时间
    //2022-02-24 13:59:59
    $res7 = DateHelper::unixtime('hour', 2, 'end');
    //获取时间戳偏移量-当前时间的两个月前的开始时间
    //2021-12-01 00:00:00 （两个月前12月，12月开始为12月1号）
    $res8 = DateHelper::unixtime('month', -2, 'begin');

    //转换为可视化的时间字符串-粗略时间估算
    //7 小时 后
    $res9 = DateHelper::human(strtotime('2022-02-24 19:00:00'));
    // 2 weeks ago
    $res10 = DateHelper::human(strtotime('2022-02-24 19:00:00'), strtotime('2022-03-14 09:00:00'), 'en');

    Debug::print_r($res1, $res2, $res3, $res4, $res5, $res6, $res7, $res8, $res9, $res10);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}