<?php
use Yjius\common\RsaHelper;
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    //获取已生成的公钥秘钥
    $rsa = new RsaHelper(file_get_contents("../files/rsa_public_key.txt"),
        file_get_contents("../files/rsa_private_key.txt"));
    $str = '这里是待加密的数据';
    echo '<hr>公钥加密私钥解密如下:<hr>';
    echo '原始数据:' . $str . '<br>';
    $tmpstr = $rsa->pubEncrypt($str); //用公钥加密
    echo '加密后的数据:' . $tmpstr, '</br>';
    $tmpstr = $rsa->privDecrypt($tmpstr); //用私钥解密
    echo '解密结果:' . $tmpstr;


    echo '<hr>私钥加密公钥解密如下:<hr>';
    echo '原始数据:', $str, '<br>';
    $tmpstr = $rsa->privEncrypt($str); //用私钥加密
    echo '加密后的数据' . $tmpstr, '</br>';
    $tmpstr = $rsa->pubDecrypt($tmpstr); //用公密解密
    echo '解密结果:' . $tmpstr, '</br>';
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}