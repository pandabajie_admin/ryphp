<?php
use Yjius\common\Debug;
use Yjius\common\PinYinHelper;

//例子
require __DIR__ . "/../../vendor/autoload.php";

try {
    //获取汉字拼音首字母
    $res1 = PinYinHelper::py("李白");

    $res2 = PinYinHelper::py("李白", true);

    Debug::print_r($res1, $res2);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
