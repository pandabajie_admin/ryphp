## Open-api

### 使用场景

- 清博常用的系统间方法调用封装。

### HotApi（清博热点）

- 需要申请api_sercet

#### hotListMap

- 清博热点首页地图数据-可见示例

#### hotList

- 清博热点首页列表数据-可见示例

#### hotListNewspaper

- 清博热点晨晚报数据-可见示例

### MemberApi（账号中心）

- 需要申请token

#### explainCookie

- 解析账号中心cookie内容是否合法 一般存于_gsdataCL下-可见示例

#### forbiddenInfo

- 查询账号是否禁用-可见示例

#### register

- 注册一个账号中心用户-自行调试

#### login

- 账号中心用户登录校验，不能用于登录使用-自行调试

#### getUserInfoById

- 用户ID查询用户信息-可见示例

#### getUserInfoByUsername

- 用户名查询用户信息-可见示例

####  getUserInfoByMobile

- 手机号查询用户信息-可见示例

#### editpassword

- 修改密码-自行调试

#### sendSms

- 发送短信-自行调试

#### sendEmail

- 发送邮件-自行调试

### YuqingOemApi（舆情oem）

#### login

- 登录到舆情-可见示例

### getUserInfoByToken

- 根据token信息获取用户详情

#### getSchemeList

- 获取企业下方案列表

#### getScheme

- 获取单个或多个方案详情

### ShudiApi（新媒体属地）

#### login

- 登录

#### logout

- 退出

#### getUserList

- 查询用户列表

#### addUser

- 添加用户

#### editUser

- 编辑用户

#### delUser

- 删除用户