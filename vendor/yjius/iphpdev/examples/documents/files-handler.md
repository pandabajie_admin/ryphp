## files-handler

### 使用场景

- 常用的PHP文件上传。
- 支持oss上传和local本地上传

### FilesHandlerBuilder 

#### uploadOneFile

- php后台上传一个文件-可见示例

#### deleteFiles

- 批量删除文件

#### getPolicy

- web前端上传获取oss签名
- 仅oss用

#### getSignUrl

- oss输出临时访问链接
- 仅oss用

#### checkFile

- 校验文件
- 本地大文件上传用

#### upload

- 本地上传
- 本地大文件上传用

#### merge

- 合并文件
- 本地大文件上传用