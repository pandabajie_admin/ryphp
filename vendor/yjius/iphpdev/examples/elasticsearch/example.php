<?php

use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //http://localhost/composer-package/examples/elasticsearch/example.php
    $defaultParams = [
        "esToken" => "",
        "esGatewayUrl" => "",
        "esScrollGatewayUrl" => "",
    ];

    $keywordGroups = [
        ['中铁', '苏州'],
        ['苏州', '復园'],
    ];
    $keywords = ['苏州復园', '中铁建熙语'];
    //例子
    $res1 = \Yjius\elasticsearch\qbBusEsBuilder\EsBuildQuery::init($defaultParams)
        ->select('news_title', 'news_uuid', 'news_postdate')
        ->whereBetween('news_postdate', ['2024-01-10', '2024-01-14'])
        ->paginator(1, 5);
    // 建议用此处
    $res2 = \Yjius\elasticsearch\qbBusEsBuilder\EsBuildQuery::init($defaultParams)
        ->select('news_title', 'news_uuid', 'news_postdate')
        ->whereBetween('news_postdate', ['2024-01-10', '2024-01-14'])
        ->paginator(1, 1);

    $res3 = \Yjius\elasticsearch\qbBusEsBuilder\EsBuildQuery::init($defaultParams)
        ->select('news_title', 'news_uuid', 'news_postdate')
        ->keywordsExclude($keywords)
        ->whereBetween('news_postdate', ['2024-01-10', '2024-01-14'])->size(4)->get();

    $res4 = \Yjius\elasticsearch\qbBusEsBuilder\EsBuildQuery::init($defaultParams)
        ->select('news_title', 'news_uuid', 'news_postdate')
        ->keywords($keywordGroups, 'title')
        ->whereBetween('news_postdate', ['2024-01-10', '2024-01-14'])->size(4)->get();

    Debug::print_r($res1, $res2, $res3, $res4);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}