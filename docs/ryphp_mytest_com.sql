/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : ryphp_mytest_com

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 13/06/2024 17:53:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ryphp_admin_dept
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_admin_dept`;
CREATE TABLE `ryphp_admin_dept`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `is_delete` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_admin_dept
-- ----------------------------
INSERT INTO `ryphp_admin_dept` VALUES (100, 0, '0', '示例公司', 0, 'admin', '15256204103', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-31 14:36:25');
INSERT INTO `ryphp_admin_dept` VALUES (101, 100, '0,100', '合肥总公司', 1, '合肥', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-31 14:36:44');
INSERT INTO `ryphp_admin_dept` VALUES (102, 100, '0,100', '北京分公司', 2, '北京', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-31 14:36:54');
INSERT INTO `ryphp_admin_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 15:59:34');
INSERT INTO `ryphp_admin_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 15:59:34');
INSERT INTO `ryphp_admin_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-31 14:37:04');
INSERT INTO `ryphp_admin_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, 'admin', '15888888888', 'yingjs@foxmail.com', '1', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-31 14:37:10');
INSERT INTO `ryphp_admin_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '1', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 15:59:36');
INSERT INTO `ryphp_admin_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 15:59:37');
INSERT INTO `ryphp_admin_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, 'admin', '15888888888', 'yingjs@foxmail.com', '0', '0', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 15:59:37');

-- ----------------------------
-- Table structure for ryphp_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_admin_menu`;
CREATE TABLE `ryphp_admin_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路由参数',
  `is_frame` int(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1025 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_admin_menu
-- ----------------------------
INSERT INTO `ryphp_admin_menu` VALUES (1, '系统管理', 0, 1, 'system', '', '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:13', '系统管理目录');
INSERT INTO `ryphp_admin_menu` VALUES (3, '系统工具', 0, 2, 'tool', '', '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-30 15:35:54', '系统工具目录');
INSERT INTO `ryphp_admin_menu` VALUES (4, 'webapi接口', 0, 3, 'http://ryphp.mytest.com/api/test/demo/demo', '', '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2024-05-27 21:21:37', 'admin', '2024-05-30 15:35:38', '若依官网地址');
INSERT INTO `ryphp_admin_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '用户管理菜单');
INSERT INTO `ryphp_admin_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '角色管理菜单');
INSERT INTO `ryphp_admin_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '菜单管理菜单');
INSERT INTO `ryphp_admin_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '部门管理菜单');
INSERT INTO `ryphp_admin_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '表单构建菜单');
INSERT INTO `ryphp_admin_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2024-05-27 21:21:37', '', '2024-05-29 16:11:18', '');
INSERT INTO `ryphp_admin_menu` VALUES (1020, '字典数据', 1, 5, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2024-06-13 16:03:50', 'admin', '2024-06-13 17:52:16', '');
INSERT INTO `ryphp_admin_menu` VALUES (1021, '字典查询', 1020, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '', 'admin', '2024-06-13 17:50:40', 'admin', '2024-06-13 17:50:40', '');
INSERT INTO `ryphp_admin_menu` VALUES (1022, '字典新增', 1020, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '', 'admin', '2024-06-13 17:51:17', 'admin', '2024-06-13 17:51:17', '');
INSERT INTO `ryphp_admin_menu` VALUES (1023, '字典编辑', 1020, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '', 'admin', '2024-06-13 17:51:39', 'admin', '2024-06-13 17:51:39', '');
INSERT INTO `ryphp_admin_menu` VALUES (1024, '字典删除', 1020, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '', 'admin', '2024-06-13 17:52:02', 'admin', '2024-06-13 17:52:02', '');

-- ----------------------------
-- Table structure for ryphp_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_admin_role`;
CREATE TABLE `ryphp_admin_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NOT NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NOT NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '角色状态（0正常 1停用）',
  `is_delete` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_admin_role
-- ----------------------------
INSERT INTO `ryphp_admin_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2024-05-27 21:21:37', '', '2024-05-30 16:53:37', '超级管理员');
INSERT INTO `ryphp_admin_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2024-05-27 21:21:37', 'admin', '2024-06-12 16:35:19', '普通角色');
INSERT INTO `ryphp_admin_role` VALUES (3, '测试角色', 'test', 3, '2', 1, 1, '0', '0', 'admin', '2024-05-30 18:02:17', 'admin', '2024-05-31 14:38:41', '测试用');
INSERT INTO `ryphp_admin_role` VALUES (4, '访客角色', 'visiter', 4, '1', 1, 1, '0', '0', 'admin', '2024-05-31 14:42:28', 'admin', '2024-05-31 14:42:28', '仅仅只能查看');

-- ----------------------------
-- Table structure for ryphp_admin_users
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_admin_users`;
CREATE TABLE `ryphp_admin_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门ID',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户账号',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户邮箱',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `is_delete` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_admin_users
-- ----------------------------
INSERT INTO `ryphp_admin_users` VALUES (1, 100, 'admin', '超级管理员', 'yingjs@foxmail.com', '15256204103', '0', '', 'a7af8f7ad2148b9f7fb1800a67d1a31b', 'bfyzJO', '0', '0', '', '2000-01-01 00:00:00', '', '2024-05-29 15:01:04', 'admin', '2024-06-12 16:35:32', '');
INSERT INTO `ryphp_admin_users` VALUES (2, 101, 'common', '普通角色', 'yingjs@foxmail.com', '15256204103', '0', '', 'e74db1155ee4908de151b5e0e0830cf6', 'joAFQT', '0', '0', '', '2000-01-01 00:00:00', '', '2024-05-29 15:01:04', 'admin', '2024-05-31 14:43:09', '');
INSERT INTO `ryphp_admin_users` VALUES (3, 103, 'test1', '测试角色', 'yingjs@foxmail.com', '18602500475', '0', '', '9a62bf0def29fa7fb4ef48f3e42b057b', '034hsA', '0', '0', '', '2000-01-01 00:00:00', 'admin', '2024-05-31 09:55:04', 'admin', '2024-05-31 14:43:25', '');
INSERT INTO `ryphp_admin_users` VALUES (4, 105, 'test2', '测试和普通角色', 'yingjs@foxmail.com', '18602500579', '0', '', '3e06345cf435b935d5860e0417d7b48e', 'bfDJLU', '0', '0', '', '2000-01-01 00:00:00', 'admin', '2024-05-31 10:04:45', 'admin', '2024-05-31 14:43:34', '');
INSERT INTO `ryphp_admin_users` VALUES (5, 109, 'test3', '访客角色', 'yingjs@foxmail.com', '18505245417', '0', '', '71dfe9285e519279680db618ab8f33e7', '145sBY', '0', '0', '', '2000-01-01 00:00:00', 'admin', '2024-05-31 10:07:42', 'admin', '2024-06-12 15:17:37', '');

-- ----------------------------
-- Table structure for ryphp_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_role_dept`;
CREATE TABLE `ryphp_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_role_dept
-- ----------------------------
INSERT INTO `ryphp_role_dept` VALUES (2, 100);
INSERT INTO `ryphp_role_dept` VALUES (2, 101);
INSERT INTO `ryphp_role_dept` VALUES (2, 105);
INSERT INTO `ryphp_role_dept` VALUES (3, 100);
INSERT INTO `ryphp_role_dept` VALUES (3, 101);
INSERT INTO `ryphp_role_dept` VALUES (3, 105);
INSERT INTO `ryphp_role_dept` VALUES (3, 106);

-- ----------------------------
-- Table structure for ryphp_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_role_menu`;
CREATE TABLE `ryphp_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_role_menu
-- ----------------------------
INSERT INTO `ryphp_role_menu` VALUES (2, 1);
INSERT INTO `ryphp_role_menu` VALUES (2, 100);
INSERT INTO `ryphp_role_menu` VALUES (2, 101);
INSERT INTO `ryphp_role_menu` VALUES (2, 102);
INSERT INTO `ryphp_role_menu` VALUES (2, 103);
INSERT INTO `ryphp_role_menu` VALUES (2, 1000);
INSERT INTO `ryphp_role_menu` VALUES (2, 1001);
INSERT INTO `ryphp_role_menu` VALUES (2, 1002);
INSERT INTO `ryphp_role_menu` VALUES (2, 1003);
INSERT INTO `ryphp_role_menu` VALUES (2, 1004);
INSERT INTO `ryphp_role_menu` VALUES (2, 1005);
INSERT INTO `ryphp_role_menu` VALUES (2, 1006);
INSERT INTO `ryphp_role_menu` VALUES (2, 1007);
INSERT INTO `ryphp_role_menu` VALUES (2, 1008);
INSERT INTO `ryphp_role_menu` VALUES (2, 1009);
INSERT INTO `ryphp_role_menu` VALUES (2, 1010);
INSERT INTO `ryphp_role_menu` VALUES (2, 1011);
INSERT INTO `ryphp_role_menu` VALUES (2, 1012);
INSERT INTO `ryphp_role_menu` VALUES (2, 1013);
INSERT INTO `ryphp_role_menu` VALUES (2, 1014);
INSERT INTO `ryphp_role_menu` VALUES (2, 1015);
INSERT INTO `ryphp_role_menu` VALUES (2, 1016);
INSERT INTO `ryphp_role_menu` VALUES (2, 1017);
INSERT INTO `ryphp_role_menu` VALUES (2, 1018);
INSERT INTO `ryphp_role_menu` VALUES (2, 1019);
INSERT INTO `ryphp_role_menu` VALUES (3, 1);
INSERT INTO `ryphp_role_menu` VALUES (3, 4);
INSERT INTO `ryphp_role_menu` VALUES (3, 100);
INSERT INTO `ryphp_role_menu` VALUES (3, 1000);
INSERT INTO `ryphp_role_menu` VALUES (3, 1001);
INSERT INTO `ryphp_role_menu` VALUES (3, 1002);
INSERT INTO `ryphp_role_menu` VALUES (4, 1);
INSERT INTO `ryphp_role_menu` VALUES (4, 4);
INSERT INTO `ryphp_role_menu` VALUES (4, 100);
INSERT INTO `ryphp_role_menu` VALUES (4, 101);
INSERT INTO `ryphp_role_menu` VALUES (4, 102);
INSERT INTO `ryphp_role_menu` VALUES (4, 103);
INSERT INTO `ryphp_role_menu` VALUES (4, 1000);
INSERT INTO `ryphp_role_menu` VALUES (4, 1007);
INSERT INTO `ryphp_role_menu` VALUES (4, 1012);
INSERT INTO `ryphp_role_menu` VALUES (4, 1016);

-- ----------------------------
-- Table structure for ryphp_system_dict
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_system_dict`;
CREATE TABLE `ryphp_system_dict`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典数据ID',
  `dict_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典标识字符串',
  `dict_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `is_delete` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_dict_key`(`dict_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_system_dict
-- ----------------------------
INSERT INTO `ryphp_system_dict` VALUES (1, '测试', 'test', '[\"tt\"]', '0', '0', '', '2000-01-01 00:00:00', '', '2000-01-01 00:00:00', '');
INSERT INTO `ryphp_system_dict` VALUES (5, '测试2', 'test2', '{\"dd\":\"dd\",\"dddd\":[\"faf\",\"fa\"]}', '1', '0', 'admin', '2024-06-13 17:33:17', 'admin', '2024-06-13 17:53:05', '备注');

-- ----------------------------
-- Table structure for ryphp_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ryphp_user_role`;
CREATE TABLE `ryphp_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ryphp_user_role
-- ----------------------------
INSERT INTO `ryphp_user_role` VALUES (1, 1);
INSERT INTO `ryphp_user_role` VALUES (2, 2);
INSERT INTO `ryphp_user_role` VALUES (3, 3);
INSERT INTO `ryphp_user_role` VALUES (4, 2);
INSERT INTO `ryphp_user_role` VALUES (4, 3);
INSERT INTO `ryphp_user_role` VALUES (5, 4);
INSERT INTO `ryphp_user_role` VALUES (7, 2);
INSERT INTO `ryphp_user_role` VALUES (7, 3);

SET FOREIGN_KEY_CHECKS = 1;
