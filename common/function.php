<?php

use yii\helpers\HtmlPurifier;

/**
 * 获取请求参数
 * @param $pname
 * @param null $default_value
 * @return array|mixed|string
 */
function loadParam($pname, $default_value = null, $from = "")
{
    $request = Yii::$app->request;
    //php字符串转大写
    if (!empty($from)) {
        $from = strtoupper($from);
        if ($from == "POST") {
            $pvalue = $request->post($pname, $default_value);
        } elseif ($from == "GET") {
            $pvalue = $request->get($pname, $default_value);
        } elseif ($from == "HEADER") {
            $headers = Yii::$app->getRequest()->getHeaders();
            $pvalue = $headers->get($pname, $default_value);
        } else {
            $pvalue = $default_value;
        }
    } else {
        $pvalue = $request->isGet ? $request->get($pname, $default_value) : $request->post($pname, $default_value);
    }
    $pvalue = is_array($pvalue) ? $pvalue : trim(HtmlPurifier::process($pvalue));
    return $pvalue;
}

function jsonSuccessReturn($data = [], $msg = 'success', $withoutlog = false)
{
    $data = [
        'code' => 10000,
        "msg" => $msg,
        "data" => $data
    ];
    $response = Yii::$app->getResponse();
    $response->format = yii\web\Response::FORMAT_JSON;
    $response->data = $data;
    return $response;
}


function jsonErrorReturn($codeName, $msg = '', $data = [], $withoutlog = false)
{
    $codeMsg = codeMsg($codeName, $msg);
    $data = [
        'code' => $codeMsg['code'],
        "msg" => $codeMsg['msg'],
        "data" => $data
    ];
    $response = Yii::$app->getResponse();
    $response->format = yii\web\Response::FORMAT_JSON;
    $response->data = $data;
    return $response;
//    header('Content-type: application/json');
//    echo json_encode($data, JSON_UNESCAPED_UNICODE);
//    die;
}

/**
 * 数字转换为中文
 * @param integer $num 目标数字
 * @return string
 */
function numberChinese($num)
{
    if (is_int($num) && $num < 100) {
        $char = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
        $unit = ['', '十', '百', '千', '万'];
        $return = '';
        if ($num < 10) {
            $return = $char[$num];
        } elseif ($num % 10 == 0) {
            $firstNum = substr($num, 0, 1);
            if ($num != 10) $return .= $char[$firstNum];
            $return .= $unit[strlen($num) - 1];
        } elseif ($num < 20) {
            $return = $unit[substr($num, 0, -1)] . $char[substr($num, -1)];
        } else {
            $numData = str_split($num);
            $numLength = count($numData) - 1;
            foreach ($numData as $k => $v) {
                if ($k == $numLength) continue;
                $return .= $char[$v];
                if ($v != 0) $return .= $unit[$numLength - $k];
            }
            $return .= $char[substr($num, -1)];
        }
        return $return;
    }
}

//检测命名空间
function getNameSpace()
{
    $namespacePath = Yii::$app->controllerNamespace;
    $nameSpace = str_replace("\controllers", "", $namespacePath);
    return $nameSpace;
}

/**
 * @param string $codeName code配置名
 * @param string $msg 自定义错误信息
 * @return array
 */
function codeMsg($codeName, $msg = '')
{
    if (!isset(Yii::$app->params['code'])) {
        return $codeMsg = [
            'code' => -100,
            'msg' => 'error'
        ];
    }
    $codeArr = Yii::$app->params['code'];
    $codeMsg = [
        'code' => -100,
        'msg' => ''
    ];
    if (isset($codeArr[$codeName]) && isset($codeArr[$codeName]['code'])) {
        $codeMsg['code'] = $codeArr[$codeName]['code'];
        $codeMsg['msg'] = $msg ? $msg : (isset($codeArr[$codeName]['msg']) ? $codeArr[$codeName]['msg'] : '');
    }
    return $codeMsg;
}

/**
 * 格式化金钱数(比如清贝、人民币等)
 * @param int|float $amount
 * @return int|string
 */
function formatMoney($amount)
{
    return $amount ? sprintf('%.2f', $amount) : 0;
}


/**
 * @param $str
 * @param string $format
 * @return bool
 */
function is_Date($str, $format = 'Y-m-d')
{
    $unixTime_1 = strtotime($str);
    if (!is_numeric($unixTime_1)) return false; //如果不是数字格式，则直接返回
    $checkDate = date($format, $unixTime_1);
    $unixTime_2 = strtotime($checkDate);
    if ($str == $checkDate) {
        return true;
    } else {
        return false;
    }
}

function checkFile($file, $extensions = null, $size = 0)
{
    if (is_array($file)) {
        if ($file['error'] === 0) {
            $fileArr = explode(".", $file["name"]);
            $extension = end($fileArr);
            if ($extensions && !in_array($extension, $extensions)) {
                return ['status' => false, 'msg' => '文件类型不正确'];
            }
            if ($size && $file['size'] > $size) {
                return ['status' => false, 'msg' => '文件大小超出限制'];
            }
            return ['status' => true, 'msg' => '验证成功'];
        } else {
            return ['status' => false, 'msg' => '文件上传失败'];
        }
    } else {

    }
}


/**
 * 读取域名
 * @return mixed
 */
function getHost()
{
    $a = explode(":", @$_SERVER['HTTP_HOST']); //$_SERVER['HTTP_HOST']:  当前请求的 Host: 头部的内容
    $domain = array_shift($a);  //删除数组第一个元素并返回被删除的元素
    if (YII_ENV == "local") { //如果是本地环境 可配置域名进行测试
        $domain = "digital.qbtest.com";
    }

    return $domain;
}

/**
 * Purpose: 获取域名+端口
 * @date 2021/4/30 10:44
 */
function getHostPort($with = 0)
{
    $a = explode(":", @$_SERVER['HTTP_HOST']); //$_SERVER['HTTP_HOST']:  当前请求的 Host: 头部的内容
    if (!empty($with)) {
        if (@$_SERVER["SERVER_PORT"] == 443) {
            return 'https://' . array_shift($a) . ':' . @$_SERVER["SERVER_PORT"];
        } else {
            return 'http://' . array_shift($a) . ':' . @$_SERVER["SERVER_PORT"];
        }
    }
    return array_shift($a) . ':' . @$_SERVER["SERVER_PORT"];  //删除数组第一个元素并返回被删除的元素
}

function changeTimeToWeek($date)
{
    $week = date("w", strtotime($date));
    switch ($week) {
        case 1:
            return "星期一";
        case 2:
            return "星期二";
        case 3:
            return "星期三";
        case 4:
            return "星期四";
        case 5:
            return "星期五";
        case 6:
            return "星期六";
        default:
            return "星期日";
    }
}


/**
 * 判断或者创建目录
 * @param string $path
 * @param bool $allDir
 * @return string
 */
function checkPath(string $path, $allDir = true)
{
    if ($allDir == true) {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    } else {
        $oriPath = dirname($path);
        if (!is_dir($oriPath)) {
            mkdir($oriPath, 0777, true);
        }
        return $oriPath;
    }
}

/**
 * 二维数组排序
 * @param $arr
 * @param $keys
 * @param string $type
 * @param int $limit
 * @return array
 */
function arraySort($arr, $keys, $type = 'asc', $limit = -1)
{
    $keysValue = $newArray = array();
    foreach ($arr as $k => $v) {
        $keysValue[$k] = $v[$keys];
    }
    if ($type == 'asc') {
        asort($keysValue);
    } else {
        arsort($keysValue);
    }
    reset($keysValue);
    foreach ($keysValue as $k => $v) {
        $newArray[] = $arr[$k];
        if ($limit != -1 && count($newArray) >= $limit) {
            break;
        }
    }
    unset($v);
    unset($keysValue);
    return $newArray;
}

// 解决系统函数pathinfo 在linux解析中文出错的问题
function path_info($file_path)
{
    $path_parts = [];
    $path_parts ['dirname'] = rtrim(substr($file_path, 0, strrpos($file_path, '/')), "/") . "/";
    $path_parts ['basename'] = ltrim(substr($file_path, strrpos($file_path, '/')), "/");
    $path_parts ['extension'] = substr(strrchr($file_path, '.'), 1);
    $path_parts ['filename'] = ltrim(substr($path_parts ['basename'], 0,
        strrpos($path_parts ['basename'], '.')), "/");
    return $path_parts;
}


function formatImportTime($str, $format = "Y-m-d")
{
    if (empty($str)) {
        return "";
    }
    if (false !== strpos($str, ".")) {
        $str = str_replace(".", "-", $str);
    }
    $time = strtotime($str);
    return $time ? date($format, $time) : "";
}


//错误提示页面
function errorHtml($text = '')
{
    return '<html>
        <head></head>
        <body style="background-color: #ccc;">
            <div style="margin: 200px auto;font-size: 30px;color: red;display: flex;justify-content: center;">
            温馨提示: ' . $text . '
            </div>
        </body>
    </html>';
}

/**
 * 二维数组根据某个key去重
 * @param  [type] $arr [description]
 * @param  [type] $key [description]
 * @return [type]      [description]
 */
function array2Unique($arr = [], $key = null)
{
    //建立一个目标数组
    $res = [];
    $existArr = [];
    foreach ($arr as $value) {
        if (!in_array($value[$key], $existArr)) {
            $existArr[] = $value[$key];
            $res[] = $value;
        }
    }
    return $res;
}

/**
 * 处理用户表格中输入的百分比, 注意：最后输出不带百分号
 * 如输入123%，我会更新为 100%；
 * 如输入0.6，我会更新为 60%；
 * 如输入0.6%，保持0.6%；
 * 如输入-0.6%，更新0.6%；
 * 如输入-08，更新8%；
 * 如输入123，更新为100%
 * !!!注意：最后输出不带百分号
 * @param $ratio
 * @return array|float|int|mixed|string|string[]
 */
function handlePercentage($ratio)
{
    if (empty($ratio)) {
        $ratio = 0;
    }
    if (strpos($ratio, '-') !== FALSE) {
        $ratio = str_replace('-', '', $ratio);
        $ratio = floatval($ratio);
    }
    if (strpos($ratio, '%') !== FALSE) {
        $ratio = str_replace('%', '', $ratio);
        if ($ratio > 100) {
            $ratio = 100;
        }
        return $ratio;
    }
    if (floatval($ratio) < 1) {
        $ratio = $ratio * 100;
    }
    if (floatval($ratio) > 100) {
        $ratio = 100;
    }
    if (floatval($ratio) == 100 || floatval($ratio) == 1) {
        $ratio = 100;
    }
    return $ratio;
}

//设置需要删除的文件夹,清空文件夹函数和清空文件夹后删除空文件夹函数的处理
function delDir($path)
{
    //如果是目录则继续
    if (is_dir($path)) {
        //扫描一个文件夹内的所有文件夹和文件并返回数组
        $p = scandir($path);
        foreach ($p as $val) {
            //排除目录中的.和..
            if ($val != "." && $val != "..") {
                //如果是目录则递归子目录，继续操作
                if (is_dir($path . $val)) {
                    //子目录中操作删除文件夹和文件
                    delDir($path . $val . '/');
                    //目录清空后删除空文件夹
                    @rmdir($path . $val . '/');
                } else {
                    //如果是文件直接删除
                    unlink($path . $val);
                }
            }
        }
    }
}

