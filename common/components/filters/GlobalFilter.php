<?php

namespace common\components\filters;

use Yii;
use yii\base\ActionFilter;
use Yjius\common\Debug;
use Yjius\common\ToolsHelper;

/**
 * 全局过滤器使用
 */
class GlobalFilter extends ActionFilter
{
    private $_startTime;

    public function beforeAction($action)
    {
        $this->_startTime = microtime(true);
        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $time = microtime(true) - $this->_startTime;
        //消耗时间  "Action '{$action->uniqueId}' spent $time second."
        //请求内容  Yii::$app->request
        //返回结果  $result->data
        if (is_object($result)) {
            $result->data['spent'] = round(intval($time * 1000), 2) . 'ms';//输出毫秒
            $result->data['requestId'] = ToolsHelper::getUuid();
        }
        return parent::afterAction($action, $result);
    }
}