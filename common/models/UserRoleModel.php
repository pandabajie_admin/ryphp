<?php

namespace common\models;


use common\core\YiiModelQueryTrait;
use Yjius\common\TreeHelper;

class UserRoleModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    protected $pk = null;

    public static function tableName()
    {
        return 'ryphp_user_role';
    }
}
