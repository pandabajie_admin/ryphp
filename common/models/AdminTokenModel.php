<?php

namespace common\models;

use common\core\YiiModelQueryTrait;

/**
 * 当前表暂未使用
 */
class AdminTokenModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_admin_token';
    }

}
