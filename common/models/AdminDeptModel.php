<?php

namespace common\models;


use common\core\YiiModelQueryTrait;
use Yjius\common\TreeHelper;

class AdminDeptModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_admin_dept';
    }

    /**
     * 找出所有上级部门
     * @param $deptId
     */
    public static function getParentDeptList($deptId, $withSelf = true)
    {
        $query = self::find()->where(['is_delete' => 0]);
        $deptList = $query->orderBy("order_num asc")->asArray()->all();
        TreeHelper::instance()->init($deptList, "parent_id");
        //获取所有父级节点数据
        $res = TreeHelper::instance()->getParents($deptId, $withSelf);
        return $res;
    }

    /**
     * 找出所有下级部门
     * @param $deptId
     */
    public static function getChildDeptList($deptId, $withSelf = true)
    {
        $query = self::find()->where(['is_delete' => 0]);
        $deptList = $query->orderBy("order_num asc")->asArray()->all();
        TreeHelper::instance()->init($deptList, "parent_id");
        //获取所有子级节点数据
        $res = TreeHelper::instance()->getChildren(1, $withSelf);
        return $res;
    }

}
