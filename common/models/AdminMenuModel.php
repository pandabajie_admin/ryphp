<?php

namespace common\models;


use common\core\YiiModelQueryTrait;

class AdminMenuModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_admin_menu';
    }

}
