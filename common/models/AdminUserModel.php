<?php

namespace common\models;


use common\core\YiiModelQueryTrait;
use Yjius\common\ToolsHelper;

class AdminUserModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_admin_users';
    }


    public static function generatePassword($password, $salt)
    {
        // 密码加密规则：md5(md5(原始密码) + 盐值)
        return md5(md5($password) . $salt);
    }

    public static function generateSalt()
    {
        return ToolsHelper::random(6);
    }

    public static function getUserDept($userId)
    {
        return self::find()->alias('a')->select(['a.dept_id', 'b.dept_name'])
            ->leftJoin(AdminDeptModel::tableName() . ' as b', 'a.dept_id=b.id')
            ->where(['a.id' => $userId])->cache(300)->asArray()->one();
    }

    public static function getUserRole($userId)
    {
        return UserRoleModel::find()->alias('a')->select(['a.role_id', 'b.role_name'])
            ->leftJoin(AdminRoleModel::tableName() . ' as b', 'a.role_id=b.id')
            ->where(['a.user_id' => $userId])->cache(300)->asArray()->all();
    }

    public static function getUserRoleNormal($userId)
    {
        //处理当前用户的角色
        $roles = UserRoleModel::find()->alias('a')
            ->select(["id as role_id", "role_name", "role_key", "data_scope"])
            ->leftJoin(AdminRoleModel::tableName() . ' as b', 'a.role_id=b.id')
            ->where(['b.is_delete' => 0, 'status' => 0, 'a.user_id' => $userId])->asArray()->all();
        return $roles;

    }

    public static function getUserMenuNormal($userId)
    {
        $roles = self::getUserRoleNormal($userId);
        $userRoles = array_column($roles, "role_key");
        //获取所在角色下的所有菜单
        if (in_array("admin", $userRoles)) {
            $roleMenu = "*";
        } else {
            //获取菜单权限
            $roleIdArr = array_column($roles, "role_id");
            $roleMenu = RoleMenuModel::find()->alias('a')
                ->select(["id as menu_id", "menu_name", "perms"])
                ->leftJoin(AdminMenuModel::tableName() . ' as b', 'a.menu_id=b.id')
                ->where(['in', 'role_id', $roleIdArr])
                ->asArray()->all();
        }
        return $roleMenu;
    }
}
