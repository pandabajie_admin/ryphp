<?php

namespace common\models;


use common\core\YiiModelQueryTrait;
use Yjius\common\TreeHelper;

class AdminRoleModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_admin_role';
    }
}
