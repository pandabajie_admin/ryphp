<?php

namespace common\models;


use common\core\YiiModelQueryTrait;

class SystemDictModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ryphp_system_dict';
    }
}
