<?php

namespace common\models;


use common\core\YiiModelQueryTrait;
use Yjius\common\TreeHelper;

class RoleMenuModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    protected $pk = null;

    public static function tableName()
    {
        return 'ryphp_role_menu';
    }
}
