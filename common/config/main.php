<?php
return [
    'timeZone' => 'Asia/Shanghai',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset'
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'service' => [
            'class' => 'common\extensions\Service'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning','info'],
                    'logFile' => '@runtime/logs/'.date('Ym').'/app-'.date('Ymd').'.log',
                ],
                [
                    'class' => 'common\extensions\LogTarget',
                    'levels' => ['info'],
                    'categories' => [
                        'yii\db\Command::query',
                        'yii\db\Command::execute'
                    ]
                ]
            ],
        ],
    ],
    //全局过滤器
    'as globalFilter' => [
        'class' => 'common\components\filters\GlobalFilter', // 引入刚刚创建的过滤器类
        // 这里可以添加额外的配置参数给过滤器，如果有需要的话
    ],
];
