<?php
switch (getenv("CACHE")) {
    case "redis_single":
    case "redis":
        $cache = [
            'class' => 'yii\redis\Cache',
            'keyPrefix' => APP_NAME . ':'
        ];
        $redis = [
            'class' => 'yii\redis\Connection',
            'hostname' => getenv("CACHE_HOSTNAME"),
            'port' => getenv("CACHE_PORT"),
            'database' => getenv("CACHE_DATABASE"),
            'password' => getenv("CACHE_PASSWORD"),
        ];
        break;
    case "file":
    default:
        $cache = [
            'class' => 'yii\caching\FileCache',
            "cachePath" => "@webapi/runtime/cache",
        ];
        $redis = null;
        break;
}

return [
    'bootstrap' => [
        'queue', // 队列组件注册到控制台
    ],
    'components' => [
        'cache' => $cache,
        'redis' => $redis,
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host=" . getenv("DB_HOST") . ";dbname=" . getenv("DB_NAME"),
            'username' => getenv("DB_USERNAME"),
            'password' => getenv("DB_PASSWORD"),
            'charset' => 'utf8mb4',
        ],
        'queue' => [
            'class' => 'yii\queue\db\Queue',
            'db' => 'db',
            'tableName' => '{{%queue}}',    //队列任务表
            'channel' => 'default',
            'mutex' => 'yii\mutex\MysqlMutex',
            'ttr' => 20,    //最大执行时间
            'attempts' => 5,    //最大尝试次数
            'as log' => 'yii\queue\LogBehavior',
        ],
    ],
];
