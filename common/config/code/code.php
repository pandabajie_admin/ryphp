<?php
/**
 * 错误码定义，然后直接调用公共方法codeMsg
 */
$code = [
    'fail' => [
        'code' => -100,
        'msg' => '操作失败'
    ],
    'paramsError' => [
        'code' => 10001,
        'msg' => '参数错误'
    ],
    'systemError' => [
        'code' => 10002,
        'msg' => '系统错误'
    ],
    'needLogin' => [
        'code' => 10003,
        'msg' => '认证错误，请先登录'
    ],
    '404' => [
        'code' => 404,
        'msg' => '接口不存在'
    ],
    'authorityError' => [
        'code' => 10005,
        'msg' => '你没有此权限'
    ],
    'illegalRequest' => [
        'code' => 10006,
        'msg' => '验签失败，请稍后再试'
    ],
];
return $code;
