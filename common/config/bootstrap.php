<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@webapi', dirname(dirname(__DIR__)) . '/webapi');
Yii::setAlias('@core', dirname(dirname(__DIR__)) . '/core');


