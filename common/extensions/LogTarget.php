<?php

/**
 *  +----------------------------------------------------------------
 *  记录DB操作Log
 *  +----------------------------------------------------------------
 * @author caozelin@gsdata.cn
 *  +----------------------------------------------------------------
 *  功能：
 *  +----------------------------------------------------------------
 */

namespace common\extensions;

use yii\log\Target;


class LogTarget extends Target
{
    public function export()
    {
        if (YII_ENV == 'pro') {
            return;
        }
        // 排除无关信息
        array_pop($this->messages);

        $sqlList = [];
        foreach ($this->messages as $message) {
            if (strpos($message[0], 'FULL COLUMNS') === false && strpos($message[0], 'database()') === false) {
                $sqlList[] = $message[0];
            }
        }
        $listContent = implode(PHP_EOL, $sqlList);
        $logContent = PHP_EOL . <<<EOL
==================SQL==================
$listContent
EOL;
        $logFile = \Yii::getAlias('@runtime/logs/' . date('Ym') . '/sql-' . date('Ymd') . '.log');
        file_put_contents($logFile, $logContent, FILE_APPEND);
    }
}
