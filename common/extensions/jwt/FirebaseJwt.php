<?php

namespace common\extensions\jwt;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class FirebaseJwt
{
    private static $secret = 'HS256';
    public static $expireTime = 3600; // 1小时
    private static $iss = 'issuer';// 签发者
    private static $sub = "subject"; // 主题
    private static $aud = "audience"; // 接收方

    // 生成 JWT
    public static function encode($payloadData = [])
    {
        $payload = [
            'iss' => self::$iss, // 签发者
            'sub' => self::$sub, // 主题
            'aud' => self::$aud, // 接收方
            'iat' => time(), // 发布时间
            'nbf' => time(), // 生效时间
            'exp' => time() + self::$expireTime, // 过期时间
            'data' => [ // 自定义数据
                $payloadData
            ],
        ];

        $jwt = JWT::encode($payload, self::$secret, 'HS256'); // 使用 HS256 算法签名
        return ['jwt' => $jwt, 'payload' => $payload];
    }

    // 解码并验证 JWT
    public static function decode($jwt)
    {
        try {
            $decoded = JWT::decode($jwt, new Key(self::$secret, 'HS256')); // 使用相同的密钥和算法验证
            // JWT 验证成功，$decoded 是一个 stdClass 对象，包含 payload 的数据
            return !empty($decoded->data) ? get_object_vars($decoded->data[0]) : [];
        } catch (\Firebase\JWT\ExpiredException $e) { // 处理过期异常
            throw new \Exception('Token has expired', 401);
        } catch (\Firebase\JWT\SignatureInvalidException $e) { // 处理签名无效异常
            throw new \Exception('Invalid signature', 401);
        } catch (\Exception $e) { // 其他异常
            throw new \Exception("Other error: " . $e->getMessage(), 401);
        }
    }

}