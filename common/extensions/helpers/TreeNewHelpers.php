<?php

namespace common\extensions\helpers;

use Yjius\common\TreeHelper;

class TreeNewHelpers extends TreeHelper
{
    /**
     *
     * 获取树状数组
     * @param string $myid 要查询的ID
     * @param string $itemprefix 前缀
     * @param bool $showChildField 最底一层的字段(childlist)数据为空时,是否保留字段(childlist)
     * @return array
     */
    public function getTreeArray($myid, $itemprefix = '', $showChildField = true, $level = 0)
    {
        $childs = $this->getChild($myid);
        $n = 0;
        $data = [];
        $number = 1;
        if ($childs) {
            $level++;
            $total = count($childs);
            foreach ($childs as $id => $value) {
                $j = $k = '';
                if ($number == $total) {
                    $j .= $this->icon[2];
                    $k = $itemprefix ? $this->nbsp : '';
                } else {
                    $j .= $this->icon[1];
                    $k = $itemprefix ? $this->icon[0] : '';
                }
                unset($value['id']);
                 unset($value['parent_id']);
                $data[$n] = $value;
                // 最底一层childlist里不为空,默认保留childlist字段
                $childlist = $this->getTreeArray($id, $itemprefix . $k . $this->nbsp, $showChildField, $level);
                if ($showChildField) {
                    if (!empty($childlist)) {
                        $data[$n]['children'] = $childlist;
                    } else {
                        $data[$n]['children'] = null;
                    }
                } else {
                    if (!empty($childlist)) {
                        $data[$n]['children'] = $childlist;
                    }
                }
                $n++;
                $number++;
            }
        }
        return $data;
    }

    //继承基类、特殊处理循环里的内容
    public function getTreeArray2($myid, $itemprefix = '', $showChildField = true, $level = 0)
    {
        $childs = $this->getChild($myid);
        $n = 0;
        $data = [];
        $number = 1;
        if ($childs) {
            $level++;
            $total = count($childs);
            foreach ($childs as $id => $value) {
                $j = $k = '';
                if ($number == $total) {
                    $j .= $this->icon[2];
                    $k = $itemprefix ? $this->nbsp : '';
                } else {
                    $j .= $this->icon[1];
                    $k = $itemprefix ? $this->icon[0] : '';
                }
                unset($value['parent_id']);
                $data[$n] = $value;
                // 最底一层childlist里不为空,默认保留childlist字段
                $childlist = $this->getTreeArray2($id, $itemprefix . $k . $this->nbsp, $showChildField, $level);
                if ($showChildField) {
                    if (!empty($childlist)) {
                        $data[$n]['children'] = $childlist;
                    } else {
                        $data[$n]['children'] = null;
                    }
                } else {
                    if (!empty($childlist)) {
                        $data[$n]['children'] = $childlist;
                    }
                }
                $n++;
                $number++;
            }
        }
        return $data;
    }

}