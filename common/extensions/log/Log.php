<?php

/**
 * Log日志记录类
 * Yii2其实自带完善的日志记录组件，考虑简单还是单独写一个轻量级的日志记录类
 */

namespace common\extensions\log;

use Yii;
use yii\base\Model;

class Log extends Model
{

    /**
     * 写入日志文件
     * @param string $file 文件名
     * @param string $msg 日志内容
     * @param boolean $time 是否记录当前时间
     * @param boolean $ip 是否记录访问者IP
     */
    public static function write($file, $msg, $userId = 0, $adminId = 0, $time = true, $ip = true)
    {
        $info = [];
        $time && $info['time'] = date('Y-m-d H:i:s');
        if ($ip) {
            //部分情况无法获得IP地址，比如用命令行模式操作
            try {
                $userIP = Yii::$app->getRequest()->userIP;
            } catch (\Exception $e) {
                $userIP = 'UNKNOW|CONSOLE';
            }
            $info['requestIp'] = $userIP;
        }
        if (!empty($userId)) {
            $info["userId"] = $userId;
        }
        if (!empty($adminId)) {
            $info["adminUserId"] = $adminId;
        }
        $info["logInfo"] = $msg;
        $dir_path = Yii::$app->basePath . '/../logs/' . date('Y-m') . '/' . date('d') . '/';
        if (!is_dir($dir_path)) {
            @mkdir($dir_path, 0777, true);
            @chmod($dir_path, 0777);
        }
        if (!file_exists($dir_path . $file)) {
            $flag = 1;//新增的话去修改文件权限
        }
        $log = json_encode($info, JSON_UNESCAPED_UNICODE);
        file_put_contents($dir_path . $file, $log . PHP_EOL, FILE_APPEND);
        if (!empty($flag)) {
            @chmod($dir_path . $file, 0777);
        }
    }
}
