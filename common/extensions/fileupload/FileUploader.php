<?php
namespace common\extensions\fileupload;

use Yjius\fileshandler\FilesHandlerBuilder;

/**
 * 文件上传
 * Date: 2022/3/23 11:53
 */
class FileUploader extends FilesHandlerBuilder
{
    private $uploadConfig = [];

    public function __construct()
    {
//        $this->uploadConfig = [
//            'driver' => 'oss',
//            'accessKeyId' => 'LTAI4G8SMep9HBH3E61pSrdF',
//            'accessKeySecret' => 'dQDeEj5VsGIrYVQOz3DLkm9AeO4H8E',
//            'endpoint' => 'oss-cn-hangzhou.aliyuncs.com',
//            'bucket' => 'bsddata',
//            'storePath' => YII_ENV == 'pro' ? 'digital_enterprise/web' : 'digital_enterprise/web_test',//存储的文件夹地址前缀
//        ];
        $this->uploadConfig = [
            'driver' => 'local',
        ];
        parent::__construct($this->uploadConfig);
    }

    /**
     * @return array
     */
    public function getUploadConfig()
    {
        return $this->uploadConfig;
    }

}
