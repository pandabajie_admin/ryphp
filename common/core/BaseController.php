<?php

namespace common\core;

use Yii;

class BaseController extends CrossController
{
    //不需要验签名的接口
    private static $noCheckSignWhiteList = [];
    //请求时效性
    private $expire = YII_ENV == 'pro' ? 600 : 86400 * 5;
    //当前使用的sign平台
    protected $signOne = [];

    public function init()
    {
        parent::init();
        try {
            $checkSign = getenv("CHECK_SIGN");
            if ($checkSign == 'true' && !in_array(Yii::$app->requestedRoute, self::$noCheckSignWhiteList)) {
                //各个渠道的验签
                $timestamp = loadParam("timestamp", "", "HEADER");
                $appKey = loadParam("appKey", "", "HEADER");
                $sign = loadParam("sign", "", "HEADER");
                //判断参数为空
                if (empty($appKey) || empty($timestamp) || empty($sign)) {
                    throw new \Exception('请传入正确验签参数');
                }
                //验签参数
                $signList = Yii::$app->params["signList"];
                if (!empty($signList)) {
                    foreach ($signList as $value) {
                        if ($value['appKey'] == $appKey) {
                            $this->signOne = $value;
                            break;
                        }
                    }
                }
                //平台记录
                if (empty($this->signOne)) {
                    throw new \Exception('请联系产品管理员开放对外接口访问权限');
                }
                if ($this->signOne['is_ban']) {
                    throw new \Exception('当前平台APPKEY请求已被管理员禁用');
                }
                //请求已过期
                if ((int)$timestamp + $this->expire < time()) {
                    throw new \Exception('请求已过期');
                }
                //加密方法-时间戳截取前前8位与secret 拼接md5加密两次
                $newSign = md5(md5($this->signOne['appSecret'] . substr($timestamp, -8)));
                if ($newSign != $sign) {
                    throw new \Exception('请求验签不通过');
                }
            }
        } catch (\Exception $e) {
            return jsonErrorReturn('illegalRequest', $e->getMessage());
        }
    }
}
