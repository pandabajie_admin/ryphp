<?php

namespace common\core;

trait YiiModelQueryTrait
{

    /**
     * Purpose: 查询默认分页数据
     * $page<1 时，查询所有数据
     */
    public static function getPageList($where = [], $field = ["a.*"], $page = 1, $limit = 10, $orderBy = "a.id desc")
    {
        $count = 0;
        // 查询数据
        $query = self::find();
        $query->select($field);
        $query->alias('a');
        $query->where($where);

        //如果是分页查询
        if ($page >= 1) {
            $count = $query->count();
            $count = intval($count);
            $offset = ($page - 1) * $limit;
            $query->offset($offset)->limit($limit);
        }

        $list = $query->orderBy($orderBy)->asArray()->all();

        return [
            'count' => $count,
            'list' => $list
        ];
    }

    /**
     * Purpose: 保存一条数据,如果没有就新增
     * $data = ["id"=>1,"name"=>"test];
     */
    public static function saveDataNoExistAdd($data = [], $where = [], $pk = "id")
    {
        if (!empty($where)) {
            $model = self::find()->where($where)->one();
        }
        if (empty($model)) {
            $model = new self();
        }
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        if ($model->save()) {
            return $model->attributes[$pk];
        } else {
            return false;
        }
    }

    /**
     * Purpose: 获取数据的map数据，方便获取数据格式
     */
    public static function getOne($where, $fields = [], $withCacheSec = 0)
    {
        $mapInfo = [];
        $query = self::find()->select($fields)->where($where);
        if (!empty($withCacheSec)) {
            $query->cache($withCacheSec);
        }
        $data = $query->asArray()->one();
        return $data;
    }

    /**
     * Purpose: 获取数据的map数据，方便获取数据格式
     */
    public static function getDataMap($where, $fields = [], $indexKey = "id", $withCacheSec = 0)
    {
        $mapInfo = [];
        $query = self::find()->select($fields)->where($where);
        if (!empty($withCacheSec)) {
            $query->cache($withCacheSec);
        }
        $data = $query->asArray()->all();
        if (!empty($data)) {
            $mapInfo = array_column($data, null, $indexKey);
        }
        return $mapInfo;
    }

    /**
     * Purpose: 每50条 批量入库
     * $data = [
     *     ["id"=>1,"name"=>"test],
     *     ["id"=>2,"name"=>"test2"]
     * ];
     */
    public static function batchInsertMain($data = [], $limitNum = 50)
    {
        $i = 0;
        $insert = $fields = [];
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                if ($key == 0) {
                    $fields = array_keys($item);
                }
                $insert[] = $item;
                $i++;
                if ($i >= $limitNum) {
                    self::batchInsertData($insert, $fields);
                    $i = 0;
                    $insert = [];
                }
            }
        }
        //剩余入库
        if (!empty($insert)) {
            self::batchInsertData($insert, $fields);
        }
        return $data;
    }

    private static function batchInsertData($insert = [], $fields = [])
    {
        if (!empty($insert) && !empty($fields)) {
            \Yii::$app->db->createCommand()->batchInsert(static::tableName(), $fields, $insert)->execute();//执行批量添加
        }
        return true;
    }

}