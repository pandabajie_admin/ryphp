<?php

namespace common\core;

class CrossController extends \yii\rest\Controller
{
    public function init()
    {
        parent::init();
        //跨域处理
        $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        header("Access-Control-Allow-Origin: " . $origin);
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS");
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Allow-Headers: sign,stime,Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since,x_requested_with,Referer");
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            \Yii::$app->response->setStatusCode(204);
            \Yii::$app->end(0);
        }
        return true;
    }
}
