<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);
return [
    'id' => 'app-webapi',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'webapi\controllers',
    //'defaultRoute' => 'user/index/index',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-webapi',
            'parsers' => [//解析提交的json格式数据
                'application/json' => 'yii\web\JsonParser',
                'text/json' => 'yii\web\JsonParser',
            ],
            "baseUrl" => "/api"
        ],
        'user' => [
            'identityClass' => '',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'ryphp-webapi',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error-handler/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'modules' => [
        'test' => [//测试用
            'class' => 'webapi\modules\test\Module',
        ],
    ],
    'params' => $params,
];
