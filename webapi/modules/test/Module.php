<?php

namespace webapi\modules\test;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'webapi\modules\test\controllers';

    public function init()
    {
        if (YII_ENV == 'pro') {
            return jsonErrorReturn('fail', '测试环境示例，正式环境无法访问');
        }
        parent::init();
    }
}