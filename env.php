<?php
/**
 * Setup application environment
 */
//引入不同的env配置
$dotenv = Dotenv\Dotenv::create(__DIR__, '.env');
$my_env = $dotenv->load();
//全局变量
defined('YII_DEBUG') or define('YII_DEBUG', getenv('YII_DEBUG') === 'true');
defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV') ?: 'local');
defined('APP_NAME') or define('APP_NAME', getenv('APP_NAME') ?: 'nofind');
defined('WEB_NAME') or define('WEB_NAME', getenv('WEB_NAME') ?: '未命名的系统');
defined('IS_CONSOLE') or define('IS_CONSOLE', false);
defined('ROOT_PATH') or define('ROOT_PATH', dirname(__FILE__));
