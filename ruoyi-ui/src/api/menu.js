import request from '@/utils/request'

// 获取路由
export const getRouters = () => {
  return request({
    url: '/api/system/index/get-routers',
    method: 'get'
  })
}
