import request from '@/utils/request'

// 查询字典配置表
export function listDict(query) {
    return request({
        url: '/api/system/dict/list',
        method: 'get',
        params: query
    })
}

// 新增角色
export function addDict(data) {
    return request({
        url: '/api/system/dict/create',
        method: 'post',
        data: data
    })
}

// 详情
export function getDict(dictId) {
    return request({
        url: '/api/system/dict/detail',
        method: 'get',
        params: { 'dictId': dictId }
    })
}

// 修改
export function updateDict(data) {
    return request({
        url: '/api/system/dict/update',
        method: 'post',
        data: data
    })
}

// 删除
export function delDict(dictId) {
    return request({
        url: '/api/system/dict/delete',
        method: 'post',
        data: { 'dictId': dictId }
    })
}

//刷新缓存
export function refreshCache() {
    return request({
        url: '/api/system/dict/refresh-cache',
        method: 'post'
    })
}
