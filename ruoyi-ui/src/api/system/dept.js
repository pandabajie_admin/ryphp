import request from '@/utils/request'

// 查询部门列表
export function listDept(query) {
  return request({
    url: '/api/system/dept/list',
    method: 'get',
    params: query
  })
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: '/api/system/dept/list-exclude',
    method: 'get',
    params: { 'deptId': deptId }
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/api/system/dept/detail',
    method: 'get',
    params: { 'deptId': deptId }
  })
}

// 新增部门
export function addDept(data) {
  return request({
    url: '/api/system/dept/create-dept',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/api/system/dept/update-dept',
    method: 'post',
    data: data
  })
}

// 删除部门
export function delDept(deptId) {
  return request({
    url: '/api/system/dept/delete-dept',
    method: 'post',
    data: { 'deptId': deptId }
  })
}
