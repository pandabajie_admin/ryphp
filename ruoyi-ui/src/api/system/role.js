import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/api/system/role/list',
    method: 'get',
    params: query
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/api/system/role/create-role',
    method: 'post',
    data: data
  })
}

// 查询角色详细
export function getRole(roleId) {
  return request({
    url: '/api/system/role/detail',
    method: 'get',
    params: { 'roleId': roleId }
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/api/system/role/update-role',
    method: 'post',
    data: data
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/api/system/role/data-scope',
    method: 'post',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status
  }
  return request({
    url: '/api/system/role/change-status',
    method: 'post',
    data: data
  })
}

// 删除角色
export function delRole(roleId) {
  return request({
    url: '/api/system/role/delete-role',
    method: 'post',
    data: { 'roleId': roleId }
  })
}

// 查询角色已授权用户列表
export function allocatedUserList(query) {
  return request({
    url: '/api/system/role/allocated-auth-user-list',
    method: 'get',
    params: query
  })
}

// 查询角色未授权用户列表
export function unallocatedUserList(query) {
  return request({
    url: '/api/system/role/unallocated-auth-user-list',
    method: 'get',
    params: query
  })
}

// 取消用户授权角色
export function authUserCancel(data) {
  return request({
    url: '/api/system/role/cancel-auth-user',
    method: 'post',
    data: data
  })
}

// 批量取消用户授权角色
export function authUserCancelAll(data) {
  return request({
    url: '/api/system/role/cancel-all-auth-user',
    method: 'post',
    data: data
  })
}

// 授权用户选择
export function authUserSelectAll(data) {
  return request({
    url: '/api/system/role/select-all-auth-user',
    method: 'post',
    data: data
  })
}

// 根据角色ID查询部门树结构
export function deptTreeSelect(roleId) {
  return request({
    url: '/api/system/role/dept-tree',
    method: 'get',
    params: { 'roleId': roleId }
  })
}
