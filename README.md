# RYPHP
## 介绍
- php实现若依框架ui的简单实现，做了大量精简；开箱即用；

## 截图
![login.png](docs%2Fimages%2Flogin.png)
![user.png](docs%2Fimages%2Fuser.png)
![menu.png](docs%2Fimages%2Fmenu.png)
![user.png](docs%2Fimages%2Fuser.png)

## 安装

- PHP>7.0+Yii2.0+Mysql+Nginx常规部署
- 利用docs文件夹下的nginx、sql、以及.env-demo文件配置即可

## 更新安装COMPOSER

```shell
composer require firebase/php-jwt --ignore-platform-reqs

composer update firebase/php-jwt --ignore-platform-reqs

composer remove qingbohf/phpdev --ignore-platform-reqs 
```

## 求赞赏
![money.jpg](docs%2Fimages%2Fmoney.jpg)