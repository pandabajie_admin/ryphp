<?php

namespace console\controllers;

use yii\console\Controller;

/**
 * 临时脚本
 * Class TempController
 * @package console\controllers
 * @date  2022/5/16 18:22
 */
class TempController extends Controller
{
    public function actionTemp()
    {
        echo "临时任务";
    }
}
